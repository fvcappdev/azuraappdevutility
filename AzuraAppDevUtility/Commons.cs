﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
//using System.ComponentModel;
//using System.Data;
using System.Windows.Forms;

using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;
//using Microsoft.HostIntegration.Drda.Common;
using System.Runtime.ConstrainedExecution;
using System.Security;
//using System.Security.Principal;



namespace AzuraAppDevUtility
{
    class Commons
    {
        //global variable to keep real login user name before impersonated login user
        public static string real_user_name;

        public static void LogUser(string formName, string action, string file_name, string provider_name,string provider_id)
        {

            string queryString = @"INSERT INTO AzuraUtilyUserAudit (UserName, DateStamp, Form, Action,file_name,provider_name,provider_id)
                                    VALUES (@UserName, GETDATE(), @Form,@action,@file_name,@provider_name,@provider_id);";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);

                command.Parameters.AddWithValue("@UserName", real_user_name);
                command.Parameters.AddWithValue("@Form", formName);
                command.Parameters.AddWithValue("@Action", action);
                command.Parameters.AddWithValue("@file_name", file_name);
                command.Parameters.AddWithValue("@provider_name", provider_name);
                command.Parameters.AddWithValue("@provider_id", provider_id);
                connection.Open();

                command.ExecuteNonQuery();

            }
        }

        public static void LogUser2(string formName, string action, string file_name, string provider_name, string provider_id, string note1, string note2, string note3)
        {

            string queryString = @"INSERT INTO AzuraUtilyUserAudit (UserName, DateStamp, Form, Action, file_name, provider_name, provider_id, note1, note2, note3)
                                    VALUES (@UserName, GETDATE(), @Form, @action, @file_name, @provider_name, @provider_id, @note1, @note2, @note3);";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);

                command.Parameters.AddWithValue("@UserName", real_user_name);
                command.Parameters.AddWithValue("@Form", formName);
                command.Parameters.AddWithValue("@Action", action);
                command.Parameters.AddWithValue("@file_name", file_name);
                command.Parameters.AddWithValue("@provider_name", provider_name);
                command.Parameters.AddWithValue("@provider_id", provider_id);
                command.Parameters.AddWithValue("@note1", note1);
                command.Parameters.AddWithValue("@note2", note2);
                command.Parameters.AddWithValue("@note3", note3);
                connection.Open();

                command.ExecuteNonQuery();

            }
        }

        /*
        public static bool CheckUser()
        {
            MessageBox.Show("real_user_name=" + real_user_name); 

            string queryString = @"select Top 1 user_name from UtilUsers 
                                    where user_name = @UserName and active = 1";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
            //using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["AppDevUtilConnString"].ConnectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);

                command.Parameters.AddWithValue("@UserName", Environment.UserName.ToString());
                connection.Open();


                SqlDataReader reader = command.ExecuteReader();

                if (!reader.HasRows)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }

        }*/


        //public Worksheet xlWorkSheet create_Excel_from_page(DataGridView dataGridView2)
        //{
        //    Excel.Application xlApp;
        //    Excel.Workbook xlWorkBook;
        //    Excel.Worksheet xlWorkSheet;
        //    object misValue = System.Reflection.Missing.Value;

        //    xlApp = new Excel.Application();
        //    if (xlApp == null)
        //    {
        //        MessageBox.Show("Excel is not properly installed!!");
        //        return;
        //    }

        //    xlWorkBook = xlApp.Workbooks.Add(misValue);
        //    xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //    int i = 0;
        //    int j = 0;

        //    for (i = 0; i <= dataGridView2.RowCount - 1; i++)
        //    {
        //        for (j = 0; j <= dataGridView2.ColumnCount - 1; j++)
        //        {
        //            DataGridViewCell cell = dataGridView2[j, i];
        //            xlWorkSheet.Cells[i + 2, j + 1] = cell.Value;
        //        }
        //    }
        // return xlWorkSheet;
        //}

        //public static bool send_email(string toUser)
        //{
        //    try
        //    {
        //        MailMessage mail = new MailMessage();
        //        SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings.Get("Email_Server"));

        //        //mail.From = new MailAddress(ConfigurationManager.AppSettings.Get("Mail_From"));
        //        mail.From = new MailAddress(Environment.UserName.ToString() + "@azuracare.com");
        //        mail.To.Add(ConfigurationManager.AppSettings.Get("Mail_To"));
        //        mail.Subject = ConfigurationManager.AppSettings.Get("Mail_Subject");
        //        mail.Body = ConfigurationManager.AppSettings.Get("Mail_Body");

        //        System.Net.Mail.Attachment attachment;

        //        //attachment = new System.Net.Mail.Attachment(file_name);

        //        //mail.Attachments.Add(attachment);

        //        SmtpServer.Port = 25;
        //        SmtpServer.EnableSsl = true;
        //        SmtpServer.UseDefaultCredentials = true;

        //        SmtpServer.Send(mail);
        //        MessageBox.Show("Email Sent!");

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.ToString());
        //        return false;
        //    }
        //}

        public static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
