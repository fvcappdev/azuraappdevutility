﻿
using System;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;
using System.Runtime.ConstrainedExecution;
using System.Security;
using System.Windows.Forms;

namespace AzuraAppDevUtility
{
    

    public class ImpersonationDemo
    {
        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword,
            int dwLogonType, int dwLogonProvider, out SafeTokenHandle phToken);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public extern static bool CloseHandle(IntPtr handle);

        

        //public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword,
        //    int dwLogonType, int dwLogonProvider, out SafeTokenHandle phToken);

        //[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        //public extern static bool CloseHandle(IntPtr handle);

        // Test harness.
        // If you incorporate this code into a DLL, be sure to demand FullTrust.
        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public static void Impersonation(string userName, string password)
        {
            SafeTokenHandle safeTokenHandle;
            try
            {
                string domainName;
                // Get the user token for the specified user, domain, and password using the
                // unmanaged LogonUser method.
                // The local machine name can be used for the domain name to impersonate a user on this machine.
               
                domainName = "AAC-LLC";

                //userName = "service.dev_ng";
                //password = "^xvb&EDg65W%$"; ^xvb&amp;EDg65W%$



                const int LOGON32_PROVIDER_DEFAULT = 0;
                //This parameter causes LogonUser to create a primary token.
                const int LOGON32_LOGON_INTERACTIVE = 2;

                // Call LogonUser to obtain a handle to an access token.
                bool returnValue = LogonUser(userName, domainName, password,
                    LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT,
                    out safeTokenHandle);

                //MessageBox.Show("LogonUser called.");

                if (false == returnValue)
                {
                    int ret = Marshal.GetLastWin32Error();
                    MessageBox.Show("LogonUser failed with error code :" +  ret);
                    throw new System.ComponentModel.Win32Exception(ret);
                }
               // MessageBox.Show("Before impersonation: " + WindowsIdentity.GetCurrent().Name);

                WindowsIdentity newId = new WindowsIdentity(safeTokenHandle.DangerousGetHandle());
                WindowsImpersonationContext impersonatedUser = newId.Impersonate();
               // MessageBox.Show("After impersonation: " + WindowsIdentity.GetCurrent().Name);

               // safeTokenHandle.Close();
                //using (safeTokenHandle)
                //{
                //    MessageBox.Show("Did LogonUser Succeed? " + (returnValue ? "Yes" : "No"));
                //    MessageBox.Show("Value of Windows NT token: " + safeTokenHandle);

                //    // Check the identity.
                //    MessageBox.Show("Before impersonation: "+ WindowsIdentity.GetCurrent().Name);
                //    // Use the token handle returned by LogonUser.


                //    using (WindowsIdentity newId = new WindowsIdentity(safeTokenHandle.DangerousGetHandle()))
                //    {
                //        using (WindowsImpersonationContext impersonatedUser = newId.Impersonate())
                //        {

                //            // Check the identity.
                //            MessageBox.Show("After impersonation: "+ WindowsIdentity.GetCurrent().Name);
                //        }
                //    }
                //    // Releasing the context object stops the impersonation
                //    // Check the identity.
                //    MessageBox.Show("After closing the context: " + WindowsIdentity.GetCurrent().Name);
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurred. " + ex.Message);
            }
        }

        public static void Dispose()
        {
            //safeTokenHandle.Dispose();
           
        }
    }
    public sealed class SafeTokenHandle : SafeHandleZeroOrMinusOneIsInvalid
    {
        private SafeTokenHandle()
            : base(true)
        {
        }

        [DllImport("kernel32.dll")]
        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
        [SuppressUnmanagedCodeSecurity]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool CloseHandle(IntPtr handle);

        protected override bool ReleaseHandle()
        {
            return CloseHandle(handle);
        }
    }

}
