﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace AzuraAppDevUtility
{
    public partial class frmAddDocToAutoFax : Form
    {
        public frmAddDocToAutoFax()
        {
            InitializeComponent();
        }
        public DataTable dt = new DataTable();

        private void frmAddDocToAutoFax_Load(object sender, EventArgs e)
        {
            //set up datagridview with named columns
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Status";
            column.Name = "Status";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Item_name";
            column.Name = "Item_name";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "provider_id";
            column.Name = "Provider ID";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "description";
            column.Name = "Description";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "last_name";
            column.Name = "Last Name";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "first_name";
            column.Name = "First Name";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "middle_name";
            column.Name = "Middle Name";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "address_line_1";
            column.Name = "Address 1";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "address_line_1";
            column.Name = "Address 2";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "city";
            column.Name = "City";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "state";
            column.Name = "State";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "zip";
            column.Name = "Zip";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "degree";
            column.Name = "Degree";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "other_lic_desc";
            column.Name = "Other";
            dataGridView1.Columns.Add(column);

            column = null;

            // get service account username password for NG
            //string userName = ConfigurationManager.AppSettings.Get("service_account_userName");
            //string password = ConfigurationManager.AppSettings.Get("service_account_password");

            //// Impersonate user
            //ImpersonationDemo.Impersonation(userName, password);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if ((txtFirstName.TextLength == 0) && (txtLastName.TextLength == 0))
            {
                MessageBox.Show("Please input values in first name or last name field!");
                return;
            }
            //MessageBox.Show("username=" + Environment.UserName.ToString());
            //MessageBox.Show("real_username=" + Commons.real_user_name);

            RefreshDataGrid();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //if (dataGridView1.SelectedRows.Count == 0)
            //{
            //    MessageBox.Show("Please select a Doc.");
            //    return;
            //}

            //MessageBox.Show("provider id=" + dataGridView1.SelectedRows[0].Cells[2].Value);

            //if (dataGridView1.SelectedRows[0].Cells[0].Value.ToString() == "Exist")
            //{
            //    MessageBox.Show("This provider already exist in auto fax table.");
            //    return;
            //}


            string queryString = @"INSERT INTO ihs_fax_documents (provider_id, item_name)
                                values (convert(uniqueidentifier,@ProviderID), 'AAC_procedure_note')";

            //MessageBox.Show("queryString=" + queryString);

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
           
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                
                command.Parameters.AddWithValue("@ProviderID", dataGridView1.SelectedRows[0].Cells[2].Value);
               // command.Parameters.Add("@ProviderID", SqlDbType.UniqueIdentifier, 16).Value = new Guid(dataGridView1.SelectedRows[0].Cells[2].ToString());

                connection.Open();

                command.ExecuteNonQuery();
            }

            // last name  + ',' +first_name
            String provider_name = dataGridView1.SelectedRows[0].Cells[4].Value.ToString() + ',' + dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
            string provider_id = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            Commons.LogUser(this.Name, "Add provider to AutoFax","",provider_name,provider_id);
            RefreshDataGrid();
        }

        private void btnDisable_Click(object sender, EventArgs e)
        {
            
            //if (dataGridView1.SelectedRows.Count == 0)
            //{
            //    MessageBox.Show("Please select a Doc");
            //    return;
            //}
         
           
            //if (dataGridView1.SelectedRows[0].Cells[0].Value.ToString() == "Not Exist")
            //{
            //    MessageBox.Show("This provider doesn't exist in auto fax table. Yoou need add this provider first.");
            //    return;
            //}
            
            string status = string.Empty;
            if ((dataGridView1.SelectedRows[0].Cells[0].Value.ToString()) == "Exist" && (dataGridView1.SelectedRows[0].Cells[1].Value.ToString() == "Disable"))
            {
                status = "AAC_procedure_note";
            }
            else
            {
                status = "Disable";
            }
           //MessageBox.Show("status=" + status);

            string queryString = @"UPDATE ihs_fax_documents
                                SET item_name = @Status
                                WHERE provider_id = (convert(uniqueidentifier,@ProviderID));";
           

           using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
           {
                SqlCommand command = new SqlCommand(queryString, connection);

                command.Parameters.AddWithValue("@Status", status);
                command.Parameters.AddWithValue("@ProviderID", dataGridView1.SelectedRows[0].Cells[2].Value);
                connection.Open();
                command.ExecuteNonQuery();
            }

            // last name  + ',' +first_name
            String provider_name = dataGridView1.SelectedRows[0].Cells[4].Value.ToString() + ',' + dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
            string provider_id = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();

            //MessageBox.Show("Provider_id =" + provider_id);
            Commons.LogUser(this.Name, "enable/disable provider to AutoFax", "", provider_name,provider_id);

            RefreshDataGrid();
        }

        private void RefreshDataGrid()
        {

            string queryString = @"SELECT  'Exist' as Status, f.item_name,m.provider_id , m.description, m.last_name, m.first_name
                                       , m.middle_name, m.address_line_1, m.address_line_2, m.city, m.state
                                       , m.zip, m.degree, m.other_lic_desc
                                       FROM provider_mstr m WITH(NOLOCK)
                                      inner join ihs_fax_documents f on m.provider_id = f.provider_id
                                      WHERE m.last_name like @lastName AND m.first_name like @FirstName 
                                     and(m.delete_ind = 'N' or m.delete_ind = '')
                                    union
                                 select 'Not Exist' as Status,
                                     '' ,  m.provider_id
                                       , m.description
                                       , m.last_name
                                       , m.first_name
                                       , m.middle_name, m.address_line_1, m.address_line_2, m.city, m.state
                                       , m.zip, m.degree, m.other_lic_desc
                                       FROM provider_mstr m WITH(NOLOCK)
                                        left join ihs_fax_documents f
                                         on m.provider_id = f.provider_id
                                         where f.provider_id is null
                                       and (m.delete_ind = 'N' or m.delete_ind = '')
                                    and m.last_name like @LastName AND m.first_name like @FirstName";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
            {
                
                SqlCommand command = new SqlCommand(queryString, connection);

                command.Parameters.AddWithValue("@LastName", txtLastName.Text + "%");
                command.Parameters.AddWithValue("@FirstName", txtFirstName.Text + "%");

                connection.Open();

                SqlDataAdapter adapt = new SqlDataAdapter(command);

                dt = new DataTable();

                adapt.Fill(dt);
                dataGridView1.DataSource = dt;

                connection.Close();

                // add row number 
                foreach (DataGridViewRow row in dataGridView1.Rows)
                    row.HeaderCell.Value = (row.Index + 1).ToString();

                
               if (dt.Rows.Count > 0)
                    button_control();
            }
            

            //if (dataGridView1.SelectedRows[0].Cells[0].Value.ToString() == "Exist")
            //    btnAdd.Enabled = false;

            //else btnAdd.Enabled = true;


        }

        private void Grid_click(object sender, EventArgs e)
        {
            //if (dataGridView1.SelectedRows[0].Cells[0].Value.ToString() == "Exist")
            //    btnAdd.Enabled = false;

            //else btnAdd.Enabled = true;

            //if (dataGridView1.SelectedRows[0].Cells[0].Value.ToString() == "Not Exist")
            //    btnDisable.Enabled = false;
            //else
            //    btnDisable.Enabled = true;
            if (dt.Rows.Count >0)
            button_control();



        }
        private void button_control()
        {
            if (dataGridView1.SelectedRows[0].Cells[0].Value.ToString() == "Exist")
                btnAdd.Enabled = false;

            else btnAdd.Enabled = true;

            if (dataGridView1.SelectedRows[0].Cells[0].Value.ToString() == "Not Exist")
                btnDisable.Enabled = false;
            else
                btnDisable.Enabled = true;

        }

        private void Form_closed(object sender, FormClosingEventArgs e)
        {
           //ImpersonationDemo.Dispose();
        }
    }
}
