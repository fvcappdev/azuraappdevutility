﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
using Excel = Microsoft.Office.Interop.Excel;
using System.Security.Cryptography.X509Certificates;
using System.IO;

using System.Diagnostics;

using System.Runtime.InteropServices;


namespace AzuraAppDevUtility
{
    public partial class frmAddGPMissingRecp : Form
    {
        public frmAddGPMissingRecp()
        {
            InitializeComponent();

            // default date to first day of last month and last day of last month
            var today = DateTime.Today;
            var month = new DateTime(today.Year, today.Month, 1);
            var first = month.AddMonths(-1);
            var last = month.AddDays(-1);

            dateTimePicker1.Value = first;
            dateTimePicker2.Value = last;
        }

        public string file_name = ConfigurationManager.AppSettings.Get("MissingGP_File_Name");
        public string fileDirectory = Path.Combine(Environment.CurrentDirectory, ConfigurationManager.AppSettings.Get("MissingGP_Folder"));
        public string fileDirectory_finance = Path.Combine(Environment.CurrentDirectory, ConfigurationManager.AppSettings.Get("MissingGP_Folder_finance"));
        string file_name_finance;
       

        public string startDate;
        public string endDate;
        public DataSet ds = new DataSet();

        private void frmAddGPMissingRecp_Load(object sender, EventArgs e)
        {
            //set up datagridview with named columns
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "POPRCTNM";
            column.Name = "POPRCTNM";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "FACILITY";
            column.Name = "FACILITY";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "PONUMBER";
            column.Name = "PONUMBER";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "RECEIPTDATE";
            column.Name = "RECEIPTDATE";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "POSTEDDT";
            column.Name = "POSTEDDT";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "VENDORID";
            column.Name = "VENDORID";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "VENDNAME";
            column.Name = "VENDNAME";
            dataGridView1.Columns.Add(column);


            //
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "POPRCTNM";
            column.Name = "POPRCTNM";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "FACILITY";
            column.Name = "FACILITY";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "PONUMBER";
            column.Name = "PONUMBER";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "RECEIPTDATE";
            column.Name = "RECEIPTDATE";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "POSTEDDT";
            column.Name = "POSTEDDT";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "VENDORID";
            column.Name = "VENDORID";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "VENDNAME";
            column.Name = "VENDNAME";
            dataGridView2.Columns.Add(column);

            column = null;


            // get service account username password
            //string userName = ConfigurationManager.AppSettings.Get("service_account_userName");
            //string password = ConfigurationManager.AppSettings.Get("service_account_password");

            //// Impersonate user
            //ImpersonationDemo.Impersonation(userName, password);
        }

        private void RefreshDataGrid()
        {
            string queryString = ConfigurationManager.AppSettings.Get("MissingGP_search_Store_prod");
            //string queryString = "PTC_Get_POReceipts_With_No_Facility";
           
            ds = new DataSet(); // reset dataet

           using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GPConnString"].ConnectionString))
           { 

                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@StartDate", startDate);
                command.Parameters.AddWithValue("@EndDate", endDate);
                
                SqlDataAdapter adapt = new SqlDataAdapter(command);
                adapt.Fill(ds);

                //MessageBox.Show("after " + queryString);
                dataGridView1.DataSource = ds.Tables[0];
                dataGridView2.DataSource = ds.Tables[1];

                foreach (DataGridViewRow row in dataGridView1.Rows)
                    row.HeaderCell.Value = (row.Index + 1).ToString();

                foreach (DataGridViewRow row2 in dataGridView2.Rows)
                    row2.HeaderCell.Value = (row2.Index + 1).ToString();
            }

            // user can't make selction 
            dataGridView1.ReadOnly = true;
            dataGridView2.ReadOnly = true;
            dataGridView1.ClearSelection();
            dataGridView2.ClearSelection();

        }

        private void DataGridView1_SelectionChanged(Object sender, EventArgs e)
        {
            dataGridView1.ClearSelection();
        }

        private void DataGridView2_SelectionChanged(Object sender, EventArgs e)
        {
            dataGridView2.ClearSelection();
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            // everytime user click search, we will reset these two values and will be used by delete function
            startDate = dateTimePicker1.Value.ToShortDateString();
            endDate =dateTimePicker2.Value.ToShortDateString();

            RefreshDataGrid();

        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            
            if (dataGridView1.RowCount == 1)
            {
                MessageBox.Show("No GP infomartion are available!");
                return;
            }

            // drop table and back up table B3970300
           
            String queryString1 = @"IF OBJECT_ID('B3970300_BAK1','U') IS NOT NULL
                                    drop table [B3970300_BAK1]
                                   select * into B3970300_BAK1 from B3970300 ";
            
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GPConnString"].ConnectionString))
                {
                
                    connection.Open();
                    SqlCommand command = new SqlCommand(queryString1, connection);
                    command.ExecuteNonQuery();

                    MessageBox.Show("Table B3970300 was backed up to [B3970300_BAK1].");
                }

            // create excel file first 
            create_Excel_files_from_DataSet();

            
            //string queryString = "PTC_Update_POReceipt_Facility";
            string queryString= ConfigurationManager.AppSettings.Get("MissingGP_update_store_prod");
            //MessageBox.Show("before " + queryString);

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GPConnString"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@StartDate", startDate);
                command.Parameters.AddWithValue("@EndDate", endDate);
               
                command.ExecuteNonQuery();
            }

            Commons.LogUser(this.Name, "Update Missing GP Receps.", file_name,"", "");
            MessageBox.Show("Missing GPS were updated!");
         
            RefreshDataGrid();
        }

        


        private void create_Excel_files_from_DataSet()
        {

            bool exists = System.IO.Directory.Exists(fileDirectory);

            if (!exists)
                System.IO.Directory.CreateDirectory(fileDirectory);

            //MessageBox.Show("fileDirectory= " + fileDirectory);


            int mon = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            int day = DateTime.Now.Day;
            file_name = file_name.Replace("MM", mon.ToString());

            file_name = file_name.Replace("YYYY", year.ToString());
            file_name = file_name.Replace("DD", day.ToString());

           file_name_finance = fileDirectory_finance + "\\" + file_name;

            file_name = fileDirectory + "\\" + file_name;

            
            //MessageBox.Show("File_name=" + file_name);

            Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet1;
               
                object misValue = System.Reflection.Missing.Value;

                xlApp = new Excel.Application();
                if (xlApp == null)
                {
                    MessageBox.Show("Excel is not properly installed!!");
                    return;
                }
               
                xlWorkBook = xlApp.Workbooks.Add(misValue);
            
               
            List<string> SheetNames = new List<string>();
                SheetNames.Add(ConfigurationManager.AppSettings.Get("MissingGP_missing_tab"));
                SheetNames.Add(ConfigurationManager.AppSettings.Get("MissingGP_issue_tab"));
               

               try
                {
                    // check how many tabs we need and add tabs from dataset ds
                    for (int i = 1; i < ds.Tables.Count; i++)
                        xlWorkBook.Worksheets.Add(); 

                     for (int i = 0; i < ds.Tables.Count; i++)
                     {

                        int r = 1; // Initialize Excel Row Start Position  = 1
                        xlWorkSheet1 = xlWorkBook.Worksheets[i + 1];

                        //Writing Columns Name in Excel Sheet

                        for (int col = 1; col < ds.Tables[i].Columns.Count; col++)

                            xlWorkSheet1.Cells[r, col] = ds.Tables[i].Columns[col - 1].ColumnName;

                        r++;

                        //Writing Rows into Excel Sheet

                        for (int row = 0; row < ds.Tables[i].Rows.Count; row++) //r stands for ExcelRow and col for ExcelColumn
                        {
                            // Excel row and column start positions for writing Row=1 and Col=1

                            for (int col = 1; col < ds.Tables[i].Columns.Count; col++)

                                xlWorkSheet1.Cells[r, col] = ds.Tables[i].Rows[row][col - 1].ToString();

                            r++;

                        }

                        xlWorkSheet1.Name = SheetNames[i];//Renaming the ExcelSheets
                     }


               


                xlWorkBook.SaveAs(file_name, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);


                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                // move file to finance folder
              
                System.IO.File.Move(file_name, file_name_finance);
               
              
                MessageBox.Show("Excel file was move to finance folder, you can find the file in " + file_name_finance);

                Commons.releaseObject(xlWorkBook.Worksheets[1]);
                Commons.releaseObject(xlWorkBook.Worksheets[2]);
                Commons.releaseObject(xlWorkBook);
                Commons.releaseObject(xlApp);

            }

            catch (Exception exHandle)

                {
                    Console.WriteLine("Exception: " + exHandle.Message);
                    Console.ReadLine();
                }

                //finally
                //{
                //    foreach (Process process in Process.GetProcessesByName("Excel"))
                //    process.Kill();
                //}
           
        }

       
    }
}
