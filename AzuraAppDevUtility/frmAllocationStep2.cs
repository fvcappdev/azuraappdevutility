﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;

namespace AzuraAppDevUtility
{
    public partial class frmAllocationStep2 : Form
    {
        public frmAllocationStep2()
        {
            InitializeComponent();
        }

        String LocnCode = "";
        String ItemNum = "";

        private void RefreshDataGrid(string LocnCode, String ItemNum)
        {

            string queryString = @"SELECT * 
                                FROM IV00102 
                                WHERE LOCNCODE = @LOCNCODE
                                AND ITEMNMBR = @ITEMNMBR

                                SELECT *
                                FROM IV00300
                                WHERE LOCNCODE = @LOCNCODE
                                AND ITEMNMBR = @ITEMNMBR";


            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GPConnString"].ConnectionString))
            {

                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@LOCNCODE", LocnCode);
                command.Parameters.AddWithValue("@ITEMNMBR", ItemNum);
                SqlDataAdapter adapt = new SqlDataAdapter(command);

                DataSet ds = new DataSet();
                adapt.Fill(ds);

                dataGridView1.DataSource = ds.Tables[0];
                dataGridView2.DataSource = ds.Tables[1];

                this.dataGridView1.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                this.dataGridView2.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                this.dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                this.dataGridView2.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;


                this.dataGridView1.AutoResizeColumns();
                //this.dataGridView1.ClipboardCopyMode =DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
                this.dataGridView1.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;

                this.dataGridView2.AutoResizeColumns();
                //this.dataGridView1.ClipboardCopyMode =DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
                this.dataGridView2.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;

                // user can't make selction 
                //dataGridView1.ReadOnly = true;
                //dataGridView2.ReadOnly = true;
                //dataGridView1.ClearSelection();
                //dataGridView2.ClearSelection();

            }
        }



        private void btnLookUp_Click(object sender, EventArgs e)
        {

            LocnCode = this.txtLocnCode.Text;
            ItemNum = this.txtItemNum.Text;

            RefreshDataGrid(LocnCode, ItemNum);

           

             

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            LocnCode = this.txtLocnCode.Text;
            ItemNum = this.txtItemNum.Text;

            UpdateTables(LocnCode, ItemNum);
        }

       


        private void UpdateTables(string LocnCode, string ItemNum)
        {

            string querystring = @"UPDATE IV
                                    SET IV.ATYALLOC = 0
                                    FROM IV00102 IV
                                    WHERE LOCNCODE = @LOCNCODE
                                    AND ITEMNMBR = @ITEMNMBR

                                    UPDATE IV2
                                    SET IV2.ATYALLOC = 0
                                    FROM IV00300 IV2 
                                    WHERE LOCNCODE = @LOCNCODE
                                    AND ITEMNMBR = @ITEMNMBR

                                    UPDATE IV2
                                    SET IV2.LTNUMSLD = CASE WHEN (QTYRECVD-QTYSOLD) = 0 THEN 1 ELSE 0 END
                                    FROM IV00300 IV2 --Lot Details
                                    WHERE LOCNCODE = @LOCNCODE
                                    AND ITEMNMBR = @ITEMNMBR";




            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GPConnString"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(querystring, connection);
                command.Parameters.AddWithValue("@LOCNCODE", LocnCode);
                command.Parameters.AddWithValue("@ITEMNMBR", ItemNum);
                SqlDataAdapter adapt = new SqlDataAdapter(command);
                command.ExecuteNonQuery();
            }
            MessageBox.Show("Two tables IV00102 and IV00300 were updated!");
            Commons.LogUser2(this.Name, "Update Item on table IV00102 and IV00300. ", "", "","","LocnCode=" + LocnCode, "ItemNum=" + ItemNum,"");
            RefreshDataGrid(LocnCode, ItemNum);
        }
        /*
        private void button1_Click(object sender, EventArgs e)
        {
            if (this.dataGridView2.GetCellCount(DataGridViewElementStates.Selected) > 0)
            {
                try
                {
                    // Add the selection to the clipboard.
                    Clipboard.SetDataObject(
                        this.dataGridView2.GetClipboardContent());

                    // Replace the text box contents with the clipboard text.
                    //this.TextBox1.Text = Clipboard.GetText();
                    MessageBox.Show(Clipboard.GetText());
                }
                catch (System.Runtime.InteropServices.ExternalException)
                {
                    //this.TextBox1.Text =
                    MessageBox.Show("The Clipboard could not be accessed. Please try again.");
                }
            }
        }*/

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
    
}
