﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;

namespace AzuraAppDevUtility
{
    public partial class frmCleanBatchFromAutpost : Form
    {
        public frmCleanBatchFromAutpost()
        {
            InitializeComponent();
        }

        String batchNumber = "";

        private void RefreshDataGrid(string batchNumber)
        {

            string queryString = @"select [GLPOSTDT]
                                    ,[BCHSOURC]
                                    ,[BACHNUMB],
	                                [MKDTOPST],
                                    [BCHSTTUS]
                                   ,[SERIES]
                                  ,[NUMOFTRX]
                                  ,[RECPSTGS]
                                  ,[DELBACH]
                                  ,[MSCBDINC]
                                  ,[BACHFREQ]
                                  ,[RCLPSTDT]
                                  ,[NOFPSTGS]
                                  ,[BCHCOMNT]
                                  ,[BRKDNALL]
                                  ,[CHKSPRTD]
                                  ,[RVRSBACH]
                                  ,[USERID]
                                  ,[CHEKBKID]
                                  ,[BCHTOTAL]
                                  ,[BACHDATE]
                                  ,[BCHSTRG1]
                                  ,[BCHSTRG2]
                                  ,[POSTTOGL]
                                  ,[MODIFDT]
                                  ,[CREATDDT]
                                  ,[NOTEINDX]
                                  ,[CURNCYID]
                                  ,[CNTRLTRX]
                                  ,[CNTRLTOT]
                                  ,[PETRXCNT]
                                  ,[APPROVL]
                                  ,[APPRVLDT]
                                  ,[APRVLUSERID]
                                  ,[ORIGIN]
                                  ,[ERRSTATE]
                                  ,[Computer_Check_Doc_Date]
                                  ,[Sort_Checks_By]
                                  ,[SEPRMTNC]
                                  ,[REPRNTED]
                                  ,[CHKFRMTS]
                                  ,[TRXSORCE]
                                  ,[PmtMethod]
                                  ,[EFTFileFormat]
                                  ,[Workflow_Approval_Status]
                                  ,[Workflow_Priority]
                                  ,[TIME1]
                                  ,[ClearRecAmts]
                                  ,[DEX_ROW_ID]
                                from SY00500
                                where BACHNUMB = @batchNum";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GPConnString"].ConnectionString))
            {

                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@batchNum", batchNumber);
                
                SqlDataAdapter adapt = new SqlDataAdapter(command);

                DataSet ds = new DataSet();
                adapt.Fill(ds);

                dataGridView1.DataSource = ds.Tables[0];

                this.dataGridView1.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                this.dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                


            }
        }


        private void btnLookup_Click(object sender, EventArgs e)
        {
            batchNumber = this.txtBatchNum.Text;

            RefreshDataGrid(batchNumber);
        }
       
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            batchNumber = this.txtBatchNum.Text;
            UpdateTables(batchNumber);
        }

        private void UpdateTables(string batchNumber)
        {

            string querystring = @"UPDATE SY00500 
                                    SET MKDTOPST=0, BCHSTTUS=0 
                                    where BACHNUMB=@batchNum";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GPConnString"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(querystring, connection);
                
                command.Parameters.AddWithValue("@batchNum", batchNumber);
                SqlDataAdapter adapt = new SqlDataAdapter(command);
                command.ExecuteNonQuery();
            }
            MessageBox.Show("Table SY00500 was updated!");
            Commons.LogUser2(this.Name, "Update Item on table SY00500. ", "", "", "", "BatchNumber=" + batchNumber,"", "");
            
            RefreshDataGrid(batchNumber);
        }
    }
}
