﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
using Excel=Microsoft.Office.Interop.Excel;
//using System.Security.Cryptography.X509Certificates;
using System.IO;

namespace AzuraAppDevUtility
{
    public partial class frmCleanNoTrans : Form
    {
        public frmCleanNoTrans()
        {
            InitializeComponent();
            
        }

        public string file_name = ConfigurationManager.AppSettings.Get("NoTran_File_Name");
        public string fileDirectory = Path.Combine(Environment.CurrentDirectory, ConfigurationManager.AppSettings.Get("NoTran_Folder"));
        public string fileDirectory_finance = Path.Combine(Environment.CurrentDirectory, ConfigurationManager.AppSettings.Get("NoTran_Folder_finance"));
        string file_name_finance;
        

        private void CleanNoTrans_Load(object sender, EventArgs e)
        {
            //set up datagridview with named columns
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "WINTYPE";
            column.Name = "WINTYPE";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "USERID";
            column.Name = "USERID";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "CMPNYNAM";
            column.Name = "CMPNYNAM";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "BCHSOURC";
            column.Name = "BCHSOURC";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "BACHNUMB";
            column.Name = "BACHNUMB";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "POSTING";
            column.Name = "POSTING";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "TRXSOURC";
            column.Name = "TRXSOURC";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "DEX_ROW_ID";
            column.Name = "DEX_ROW_ID";
            dataGridView1.Columns.Add(column);

        
            //
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "GLPOSTDT";
            column.Name = "GLPOSTDT";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "BCHSOURC";
            column.Name = "BCHSOURC";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "BACHNUMB";
            column.Name = "BACHNUMB";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "SERIES";
            column.Name = "SERIES";
            dataGridView2.Columns.Add(column);


            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "NUMOFTRX";
            column.Name = "NUMOFTRX";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "BCHTOTAL";
            column.Name = "BCHTOTAL";
            dataGridView2.Columns.Add(column);

            column = null;

            //string userName = ConfigurationManager.AppSettings.Get("service_account_userName");
            //string password = ConfigurationManager.AppSettings.Get("service_account_password");

            //// Impersonate user
            //ImpersonationDemo.Impersonation(userName, password);
            RefreshDataGrid();
          

        }

        private void RefreshDataGrid()
        {

            string queryString = @"select act.WINTYPE
                 ,act.USERID
                ,act.CMPNYNAM
                 ,act.BCHSOURC
                 ,act.BACHNUMB
                 ,act.POSTING
                ,act.TRXSOURC
                ,act.DEX_ROW_ID
                                from SY00500 bh
                                join DYNAMICS.dbo.SY00800 act 
                                on   act.BCHSOURC = bh.BCHSOURC
                                and  act.BACHNUMB = bh.BACHNUMB
                                left join POP10300 rh
                                 on   rh.BACHNUMB = bh.BACHNUMB
                                and  rh.BCHSOURC = bh.BCHSOURC
                                where bh.BCHSOURC = 'Rcvg Trx Ivc'
                                and rh.POPRCTNM is null

            select bh.GLPOSTDT
                                     ,bh.BCHSOURC
                                      ,bh.BACHNUMB
                                     ,bh.SERIES
                                    ,bh.NUMOFTRX
                                    ,bh.BCHTOTAL
                        from SY00500 bh
                        left join POP10300 rh
                    on rh.BACHNUMB = bh.BACHNUMB
                    and rh.BCHSOURC = bh.BCHSOURC
                where bh.BCHSOURC = 'Rcvg Trx Ivc'
                and rh.POPRCTNM is null";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GPConnString"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                SqlDataAdapter adapt = new SqlDataAdapter(command);
                
                DataSet ds = new DataSet();
                adapt.Fill(ds);

                dataGridView1.DataSource = ds.Tables[0];
                dataGridView2.DataSource = ds.Tables[1];


                foreach (DataGridViewRow row in dataGridView1.Rows)
                    row.HeaderCell.Value = (row.Index + 1).ToString();

                foreach (DataGridViewRow row2 in dataGridView2.Rows)
                    row2.HeaderCell.Value = (row2.Index + 1).ToString();

                // user can't make selction 
                dataGridView1.ReadOnly = true;
                dataGridView2.ReadOnly = true;
                dataGridView1.ClearSelection();
                dataGridView2.ClearSelection();

            }


        }
        private void DataGridView1_SelectionChanged(Object sender, EventArgs e)
        {
            dataGridView1.ClearSelection();
        }

        private void DataGridView2_SelectionChanged(Object sender, EventArgs e)
        {
            dataGridView2.ClearSelection();
        }
        //private void BtnDeleteUser_Click(object sender, EventArgs e)
        //{
        //    if (dataGridView1.RowCount == 1)
        //    {
        //        MessageBox.Show("No user activities available!");
        //        return;
        //    }


        //    string queryString = @"delete act
        //                        from SY00500 bh
        //                        join dbo.SY00800 act 
        //                         on   act.BCHSOURC = bh.BCHSOURC
        //                            and  act.BACHNUMB = bh.BACHNUMB
        //                        left join POP10300 rh
        //                        on   rh.BACHNUMB = bh.BACHNUMB
        //                        and  rh.BCHSOURC = bh.BCHSOURC
        //                        where bh.BCHSOURC = 'Rcvg Trx Ivc'
        //                           and rh.POPRCTNM is null";

        //    using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GPConnString"].ConnectionString))
        //    {
        //        SqlCommand command = new SqlCommand(queryString, connection);
        //        connection.Open();

        //        command.ExecuteNonQuery();
        //    }

        //    Commons.LogUser(this.Name, "Delete user activity.");
        //    MessageBox.Show("User activities deleted!");

        //    RefreshDataGrid();
        //}

        private void BtnDeleteTrans_Click(object sender, EventArgs e)
        {
            // first delete user activity 
            //if (dataGridView1.RowCount == 1)
            //{
            //    MessageBox.Show("No user activities available!");
            //    return;
            //}
            // make sure have user activities
               if (dataGridView1.RowCount > 1) 
               {
                   //this one removes batch activity for batches with no trx - make sure nobody in GP
                   string queryString = @"delete act
                                   from SY00500 bh
                                   join dbo.SY00800 act 
                                    on   act.BCHSOURC = bh.BCHSOURC
                                       and  act.BACHNUMB = bh.BACHNUMB
                                   left join POP10300 rh
                                   on   rh.BACHNUMB = bh.BACHNUMB
                                   and  rh.BCHSOURC = bh.BCHSOURC
                                   where bh.BCHSOURC = 'Rcvg Trx Ivc'
                                      and rh.POPRCTNM is null";


                   using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GPConnString"].ConnectionString))
                   {
                       SqlCommand command = new SqlCommand(queryString, connection);
                       connection.Open();

                       command.ExecuteNonQuery();
                   }

                  // Commons.LogUser(this.Name, "Delete user activity.", "", "", "");
                   MessageBox.Show("User activities deleted!");
               }

               // make sure have batch available
               if (dataGridView2.RowCount == 1)
               {
                   MessageBox.Show("No batch available!");
                   return;
               }

               // second,drop table and back up table SY00500

               String queryString1 = @"IF OBJECT_ID('[SY00500_Bak1]','U') IS NOT NULL
                                       drop table [SY00500_Bak1]
                                       select * into SY00500_Bak1 from SY00500 ";

               using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GPConnString"].ConnectionString))
              {
                   connection.Open();
                   SqlCommand command = new SqlCommand(queryString1, connection);
                   command.ExecuteNonQuery();

                   MessageBox.Show("Table SY00500 was backed up to SY00500_BAK1.");
               }

               
            bool exists = System.IO.Directory.Exists(fileDirectory);
            

            // third,create excel file first 
            create_Excel_from_page();

            // fourthth,  this will remove the batches with no trx in work tables.

             string queryString2 = @"delete bh
                             from SY00500 bh
                             left join POP10300 rh
                             on   rh.BACHNUMB = bh.BACHNUMB
                              and  rh.BCHSOURC = bh.BCHSOURC
                     where bh.BCHSOURC = 'Rcvg Trx Ivc'
                         and rh.POPRCTNM is null";


             using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GPConnString"].ConnectionString))
             {
                 SqlCommand command = new SqlCommand(queryString2, connection);
                 connection.Open();

                 command.ExecuteNonQuery();
             }


             Commons.LogUser(this.Name, "Delete batches with no trans.", file_name, "","");
             
            MessageBox.Show("Batches were deleted!");
          
            RefreshDataGrid();
        }


        /*private void send_emil()
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings.Get("Email_Server"));

                //mail.From = new MailAddress(ConfigurationManager.AppSettings.Get("Mail_From"));
                mail.From = new MailAddress(Environment.UserName.ToString() + "@azuracare.com");
                mail.To.Add(ConfigurationManager.AppSettings.Get("NoTran_Mail_To"));
                mail.Subject = ConfigurationManager.AppSettings.Get("NoTran_Mail_Subject");
                mail.Body = ConfigurationManager.AppSettings.Get("NoTran_Mail_Body");


                //MessageBox.Show("from" + mail.From);
                //MessageBox.Show("to");
                System.Net.Mail.Attachment attachment;

                attachment = new System.Net.Mail.Attachment(file_name);
                //MessageBox.Show("file_name =" + file_name);
                mail.Attachments.Add(attachment);

                SmtpServer.Port = 25;
                SmtpServer.EnableSsl = true;
                SmtpServer.UseDefaultCredentials = true;
                

                //MessageBox.Show("before Email Send");
                SmtpServer.Send(mail);
                MessageBox.Show("Email Sent!");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }*/




        private void create_Excel_from_page()
        {

           
            bool exists = System.IO.Directory.Exists(fileDirectory);
          

            if (!exists)
                System.IO.Directory.CreateDirectory(fileDirectory);

            int mon = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            int day = DateTime.Now.Day;
           

            file_name = file_name.Replace("MM", mon.ToString());

            file_name = file_name.Replace("YYYY", year.ToString());
            file_name = file_name.Replace("DD", day.ToString());

           file_name_finance = fileDirectory_finance + "\\" + file_name;

            file_name = fileDirectory + "\\" + file_name;

            //MessageBox.Show("file_name=" + file_name);
            //MessageBox.Show("file_name_dist=" + file_name_finance);
           
            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            
            xlApp = new Excel.Application();
            if (xlApp == null)
            {
                       MessageBox.Show("Excel is not properly installed!!");
                        return;
             }

             xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            int i = 0;
            int j = 0;

            for (i = 0; i <= dataGridView2.RowCount - 1; i++)
            {
                for (j = 0; j <= dataGridView2.ColumnCount - 1; j++)
                {
                    DataGridViewCell cell = dataGridView2[j, i];
                    xlWorkSheet.Cells[i + 2, j + 1] = cell.Value;
                }
            }
            // input colume names:
            xlWorkSheet.Cells[1, 1] = "GLPOSTDT";
            xlWorkSheet.Cells[1, 2] = "BCHSOURC";
            xlWorkSheet.Cells[1, 3] = "BACHNUMB";
            xlWorkSheet.Cells[1, 4] = "SERIES";
            xlWorkSheet.Cells[1, 5] = "NUMOFTRX";
            xlWorkSheet.Cells[1, 6] = "BCHTOTAL";

            

            xlWorkBook.SaveAs(file_name, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
           // xlWorkBook.SaveCopyAs(file_name);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            Commons.releaseObject(xlWorkSheet);
            Commons.releaseObject(xlWorkBook);
            Commons.releaseObject(xlApp);

            
            //MessageBox.Show("file_backup=" + file_backup);
          // move file to finance folder
            System.IO.File.Move(file_name, file_name_finance);
            MessageBox.Show("Excel file created , you can find the file in " + file_name_finance);
                


        }

        private void D(object sender, EventArgs e)
        {

        }
    }
}
