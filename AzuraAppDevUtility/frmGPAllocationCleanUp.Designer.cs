﻿
namespace AzuraAppDevUtility
{
    partial class frmGPAllocationCleanUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.btnGetListOfAllocation = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnAssignTrans = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.textBatchNum = new System.Windows.Forms.TextBox();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCopyListOfAllocation = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(75, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(314, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Step 1: Clear Locked Transactions";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnCleanLockedTrans_Click);
            // 
            // btnGetListOfAllocation
            // 
            this.btnGetListOfAllocation.Location = new System.Drawing.Point(75, 41);
            this.btnGetListOfAllocation.Name = "btnGetListOfAllocation";
            this.btnGetListOfAllocation.Size = new System.Drawing.Size(314, 23);
            this.btnGetListOfAllocation.TabIndex = 1;
            this.btnGetListOfAllocation.Text = "Step 2: Get List of Allocation";
            this.btnGetListOfAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGetListOfAllocation.UseVisualStyleBackColor = true;
            this.btnGetListOfAllocation.Click += new System.EventHandler(this.btnGetListOfAllocation_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(75, 96);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(788, 159);
            this.dataGridView1.TabIndex = 2;
            // 
            // btnAssignTrans
            // 
            this.btnAssignTrans.Location = new System.Drawing.Point(75, 431);
            this.btnAssignTrans.Name = "btnAssignTrans";
            this.btnAssignTrans.Size = new System.Drawing.Size(314, 23);
            this.btnAssignTrans.TabIndex = 3;
            this.btnAssignTrans.Text = "Step 4: Assign inventory transaction to location code";
            this.btnAssignTrans.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAssignTrans.UseVisualStyleBackColor = true;
            this.btnAssignTrans.Click += new System.EventHandler(this.btnAssignTrans_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(535, 490);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(214, 23);
            this.btnUpdate.TabIndex = 5;
            this.btnUpdate.Text = "Update Batch Number on table SY00500";
            this.btnUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(75, 280);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(788, 92);
            this.dataGridView2.TabIndex = 6;
            // 
            // textBatchNum
            // 
            this.textBatchNum.Location = new System.Drawing.Point(336, 493);
            this.textBatchNum.Name = "textBatchNum";
            this.textBatchNum.Size = new System.Drawing.Size(178, 20);
            this.textBatchNum.TabIndex = 7;
            this.textBatchNum.TextChanged += new System.EventHandler(this.textBatchNum_TextChanged);
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(75, 551);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(788, 40);
            this.dataGridView3.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(75, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "List of allocation:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(78, 262);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(206, 13);
            this.label2.TabIndex = 13;
            this.label2.Tag = "";
            this.label2.Text = "Table IV10000 with different item number: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(75, 535);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(176, 13);
            this.label3.TabIndex = 14;
            this.label3.Tag = "";
            this.label3.Text = "Batch information in table SY00500:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(72, 470);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(549, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Step 5: In GP production, use result from step 2, pass batch number, locncode, st" +
    "atus. If you get error, go to step 6.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(72, 496);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(258, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Step 6: Input batch number in the test box on the left:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // btnCopyListOfAllocation
            // 
            this.btnCopyListOfAllocation.Location = new System.Drawing.Point(75, 392);
            this.btnCopyListOfAllocation.Name = "btnCopyListOfAllocation";
            this.btnCopyListOfAllocation.Size = new System.Drawing.Size(467, 23);
            this.btnCopyListOfAllocation.TabIndex = 17;
            this.btnCopyListOfAllocation.Text = "Step 3: Copy allocation to excel file";
            this.btnCopyListOfAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCopyListOfAllocation.UseVisualStyleBackColor = true;
            this.btnCopyListOfAllocation.Click += new System.EventHandler(this.btnCopyListOfAllocation_Click);
            // 
            // frmGPAllocationCleanUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 595);
            this.Controls.Add(this.btnCopyListOfAllocation);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView3);
            this.Controls.Add(this.textBatchNum);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnAssignTrans);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnGetListOfAllocation);
            this.Controls.Add(this.button1);
            this.Name = "frmGPAllocationCleanUp";
            this.Text = "GP Allocation Clean Up";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnGetListOfAllocation;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnAssignTrans;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.TextBox textBatchNum;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCopyListOfAllocation;
    }
}