﻿  using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;

namespace AzuraAppDevUtility
{
    public partial class frmGPAllocationCleanUp : Form
    {
        public frmGPAllocationCleanUp()
        {
            InitializeComponent();
            
    }


        public DataTable dt = new DataTable();
        public String SY005_batchNum ;

        


        private void RefreshDataGrid(string batchnum)
        {

            string queryString = @"SELECT DISTINCT IV.BACHNUMB,
                                    IV1.IVDOCNBR [DocNumber],
                                    IV1.TRXLOCTN [LOCNCODE],
                                    IV.DOCDATE,
                                    CASE M.STATUS WHEN 3 THEN 'Post'
                                    WHEN 1 THEN 'Delete'
                                    Else 'Error' End [Status]  
                                    ,B2.BACHNUMB as B3900900_BACHNUMB
                                    ,RTRIM(B2.BSSI_Facility_ID) as B3900900_BSSI_Facility_ID
                                    ,B3.IVDOCNBR as B3910000_IVDOCNBR
                                    ,RTRIM(B3.BSSI_Facility_ID) as B3910000_BSSI_Facility_ID
                                    
                                    FROM IV10001 IV1
                            INNER JOIN IV10000 IV 
                                      ON IV1.IVDOCNBR=IV.IVDOCNBR
                            LEFT JOIN DYNAMICS..SY00800 S8 ON IV.BACHNUMB=S8.BACHNUMB
                            LEFT JOIN B3910001 B1 ON IV1.IVDOCNBR=B1.IVDOCNBR
                            LEFT JOIN MDS_TRXLOG_MASTER M ON IV1.IVDOCNBR=M.DOCNUMBER
                            left join B3900900 B2 on IV.BACHNUMB= B2.BACHNUMB and B2.BSSI_Facility_ID=IV1.TRXLOCTN 
                            left join B3910000 B3 on B3.IVDOCNBR=IV1.IVDOCNBR and B3.BSSI_Facility_ID=IV1.TRXLOCTN 
                            WHERE TRXQTY < 0 
                            AND IV.DOCDATE<CONVERT(DATE,GETDATE())
                           
                            UNION ALL
                            SELECT DISTINCT P1.BACHNUMB,
                            P1.POPRCTNM [DocNumber],
                            P2.LOCNCODE,
                            P1.receiptdate [DOCDATE],
                            CASE M.STATUS WHEN 3 THEN 'Post'
                            WHEN 1 THEN 'Delete'
                            Else 'Error' End [Status],
                            '',
                            '',
                            '',
                            ''
                            FROM POP10300 P1
                            INNER JOIN POP10310 P2 ON P1.POPRCTNM=P2.POPRCTNM
                            LEFT JOIN DYNAMICS..SY00800 S8 ON P1.BACHNUMB=S8.BACHNUMB
                            LEFT JOIN MDS_TRXLOG_MASTER M ON P1.POPRCTNM=M.DOCNUMBER
                            WHERE P1.receiptdate<CONVERT(DATE,GETDATE())
                            AND P1.BACHNUMB LIKE 'RVG%'
                            --AND P2.ITEMNMBR = '39207-12047'
                            --AND P2.LOCNCODE = '3045'
                            --AND M.STATUS = 3
                            GROUP BY P1.BACHNUMB,P1.POPRCTNM,P2.LOCNCODE,P1.receiptdate, M.STATUS
                            ORDER BY BACHNUMB, DOCNUMBER, LOCNCODE ASC

                             SELECT
                            IV.BACHNUMB,
                            IV.IVDOCNBR,
                            IV1.LNSEQNBR,
                            IV2.LNSEQNBR,
                            RTRIM(IV1.ITEMNMBR) as IV10001_ITEMNBR,
                            RTRIM(IV2.ITEMNMBR) as IV10002_ITEMNBR,
                            CASE 
                                when    IV1.ITEMNMBR = IV2.ITEMNMBR then 'Item Number is same'
                                when IV1.ITEMNMBR<> IV2.ITEMNMBR then 'Item Number is not same'
                            end as itemNbr_compare
                            FROM IV10000 IV
                            INNER JOIN IV10001 IV1 ON IV.IVDOCNBR = IV1.IVDOCNBR
                            INNER JOIN IV10002 IV2 ON IV.IVDOCNBR = IV2.IVDOCNBR AND IV1.LNSEQNBR = IV2.LNSEQNBR
                            AND(IV1.ITEMNMBR <> IV2.ITEMNMBR)
                            WHERE TRXQTY< 0
                            AND IV.DOCDATE < CONVERT(DATE, GETDATE())
                            order by IV.BACHNUMB

                            select [GLPOSTDT] ,[BCHSOURC] ,[BACHNUMB],BCHSTTUS, MKDTOPST
                            from [dbo].[SY00500]
                            where BACHNUMB=@BACHNUMB";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GPConnString"].ConnectionString))
            {

                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@BACHNUMB", batchnum);
                SqlDataAdapter adapt = new SqlDataAdapter(command);

                DataSet ds = new DataSet();
                adapt.Fill(ds);

                // below add new column "num" in fron t of table to count 
                DataColumn col1 = new DataColumn();
                col1.ColumnName = "num";

                DataColumn col2 = new DataColumn();
                col2.ColumnName = "num";
                //col1.AutoIncrement = true;
                //col1.AutoIncrementSeed = 1;
                //col1.AutoIncrementStep = 1;

                // add to first table:
                ds.Tables[0].Columns.Add(col1);
                col1.SetOrdinal(0);

                // add to second table:
                ds.Tables[1].Columns.Add(col2);
                col2.SetOrdinal(0);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["num"] = i + 1;
                }

                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    ds.Tables[1].Rows[i]["num"] = i + 1;
                }

              
                dataGridView1.DataSource = ds.Tables[0];
                dataGridView2.DataSource = ds.Tables[1];
                dataGridView3.DataSource = ds.Tables[2];

                /*
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    row.HeaderCell.Value = (row.Index + 1).ToString();
                    //row.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                }

               // column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                foreach (DataGridViewRow row2 in dataGridView2.Rows)
                    row2.HeaderCell.Value = (row2.Index + 1).ToString();
                */
                this.dataGridView1.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                this.dataGridView2.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                this.dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                this.dataGridView2.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                //this.dataGridView1.ClipboardCopyMode =DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
                this.dataGridView1.AutoResizeColumns();
                this.dataGridView1.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;


                // user can't make selction 
                //dataGridView1.ReadOnly = true;
                //dataGridView2.ReadOnly = true;
                //dataGridView1.ClearSelection();
                //dataGridView2.ClearSelection();

            }
        }


        private void btnGetListOfAllocation_Click(object sender, EventArgs e)
        {

            SY005_batchNum = this.textBatchNum.Text;
            RefreshDataGrid(SY005_batchNum);
        }

        private void btnAssignTrans_Click(object sender, EventArgs e)
        {
            string prod_name = ConfigurationManager.AppSettings.Get("GP_Allocation_CleanUp_store_prod");
            string BatchNum, Locncode;
            int i;

            for (i = 0; i <= dataGridView1.RowCount - 2; i++)

            {
                BatchNum = dataGridView1.Rows[i].Cells[1].Value.ToString();
                Locncode = dataGridView1.Rows[i].Cells[3].Value.ToString();


                if (BatchNum.ToUpper().IndexOf("TRN") >= 0)
                AssignSingleTrans(BatchNum, Locncode, prod_name);

                BatchNum = "";
                Locncode = "";
            }


          
            MessageBox.Show("Inventory transaction were assigned.");
            Commons.LogUser(this.Name, "Assigned inventory transaction.", "", "", "");

            RefreshDataGrid(SY005_batchNum);
        }

        private void AssignSingleTrans(string batchnum, string locncode, string proc_name)
        {

            
            //MessageBox.Show("before " + queryString);
          

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GPConnString"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(proc_name, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@BACHNUMB", batchnum);
                command.Parameters.AddWithValue("@LOCNCODE", locncode);

                command.ExecuteNonQuery();
            }


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

            SY005_batchNum = this.textBatchNum.Text;
            //MessageBox.Show("batchNum " + SY005_batchNum);
            string queryString = @"UPDATE dbo.SY00500 
                                SET BCHSTTUS = 0, MKDTOPST = 0
                                WHERE BACHNUMB = @batchnum";

           

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GPConnString"].ConnectionString))

            {
                SqlCommand command = new SqlCommand(queryString, connection);

                command.Parameters.AddWithValue("@batchnum", this.textBatchNum.Text);
               
                connection.Open();

                command.ExecuteNonQuery();
            }

            
            Commons.LogUser(this.Name, " Batchnum  " + SY005_batchNum + " was update in SY00500.", "", "", "");
            MessageBox.Show("Batchnum =" + SY005_batchNum + " was updated in table SY00500.");
            RefreshDataGrid(SY005_batchNum);
         
            
        }


        private void btnCleanLockedTrans_Click(object sender, EventArgs e)
        {

                string queryString = @"DELETE FROM DYNAMICS..Activity
                                        WHERE SQLSESID NOT IN (
                                        SELECT SQLSESID
                                        FROM DYNAMICS..Activity A
                                        INNER JOIN tempdb..DEX_SESSION S
                                        ON A.SQLSESID = S.session_id
                                        INNER JOIN master..sysprocesses P
                                        on S.sqlsvr_spid = P.spid
                                        AND A.USERID = P.loginame
                                        )

                                        DELETE FROM tempdb..DEX_LOCK
                                        WHERE session_id NOT IN (
                                        SELECT SQLSESID
                                        FROM DYNAMICS..ACTIVITY
                                        )

                                        DELETE FROM tempdb..DEX_SESSION
                                        WHERE session_id NOT IN (
                                        SELECT SQLSESID
                                        FROM DYNAMICS..ACTIVITY
                                        )

                                        DELETE FROM DYNAMICS..SY00800
                                        WHERE DEX_ROW_ID NOT IN (
                                        SELECT B.DEX_ROW_ID
                                        FROM DYNAMICS..SY00800 B
                                        INNER JOIN DYNAMICS..ACTIVITY A
                                        ON B.USERID = A.USERID
                                        AND B.CMPNYNAM = A.CMPNYNAM
                                        )

                                        DELETE FROM DYNAMICS..SY00801
                                        WHERE DEX_ROW_ID NOT IN (
                                        SELECT R.DEX_ROW_ID
                                        FROM DYNAMICS..SY00801 R
                                        INNER JOIN (DYNAMICS..ACTIVITY A
                                        INNER JOIN DYNAMICS..SY01500 C
                                        ON A.CMPNYNAM = C.CMPNYNAM) 
                                        ON R.USERID = A.USERID AND R.CMPANYID = C.CMPANYID)";       

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GPConnString"].ConnectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                command.ExecuteNonQuery();
            }
            
            MessageBox.Show("Locked transactions were cleanned!");

        }

        private void textBatchNum_TextChanged(object sender, EventArgs e)
        {
        }

       private void view1_cell_click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void btnCopyListOfAllocation_Click(object sender, EventArgs e)
        {
            /* this code is for copy list to clipboard
            if (this.dataGridView1.GetCellCount(DataGridViewElementStates.Selected) > 0)
            {
                try
                {
                    // Add the selection to the clipboard.
                    Clipboard.SetDataObject(
                        this.dataGridView1.GetClipboardContent());

                    MessageBox.Show("You can paste it to excel file.");
                }
                catch (System.Runtime.InteropServices.ExternalException)
                {

                    MessageBox.Show("The Clipboard could not be accessed. Please try again.");
                }
            }*/
            string file_name = ConfigurationManager.AppSettings.Get("GP_Allocation_CleanUp_File_Name");
            string dest_dir = ConfigurationManager.AppSettings.Get("GP_Allocation_CleanUp_Dest_Directory");

            bool exists = System.IO.Directory.Exists(dest_dir);

            if (!exists)
                System.IO.Directory.CreateDirectory(dest_dir);

            int year = DateTime.Now.Year;
            string month = DateTime.Now.Month.ToString();
            string day = DateTime.Now.Day.ToString();
            string strSheetName = month + '.' + day + "_" + year;

            file_name = file_name.Replace("Year", year.ToString());
            file_name = dest_dir + "\\" + file_name;

            //MessageBox.Show("file_name=" + file_name);
          
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            Excel.Sheets sheets = null;
            //Excel.Worksheet newSheet = null;

            object misValue = System.Reflection.Missing.Value;
            xlApp = new Excel.Application();

            if (xlApp == null)
            {
                MessageBox.Show("Excel is not properly installed!!");
                return;
            }

            FileInfo file = new FileInfo(file_name);
            if (file.Exists)    /* open exist file*/
            {
                   
                    /*display the excel operations including opening and saving file excelApp.Visible = true;*/
                    xlApp.DisplayAlerts = false;
                    xlWorkBook = xlApp.Workbooks.Open(file_name, ReadOnly: false, Editable: true);
                    /*activate the workbook to remove ambiguity between method _Workbook.Activate warning type cast*/
                    ((Microsoft.Office.Interop.Excel._Workbook)xlWorkBook).Activate();

                    sheets = xlWorkBook.Sheets;
                    /*add the worksheet at the end of the other worksheet*/
                    xlWorkSheet = (Excel.Worksheet)sheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);
                    xlWorkSheet.Name = strSheetName;
                    ((Microsoft.Office.Interop.Excel._Worksheet)xlWorkSheet).Activate();

                    int i = 0;
                    int j = 0;

                    for (i = 0; i <= dataGridView1.RowCount - 2; i++)
                    {
                        for (j = 1; j <= 5; j++)
                        {
                        DataGridViewCell cell = dataGridView1[j, i];
                        xlWorkSheet.Cells[i + 2, j] = cell.Value;
                        }
                    }
                // input colume names:
                xlWorkSheet.Cells[1, 1] = "BACHNUMB";
                xlWorkSheet.Cells[1, 2] = "DocNumber";
                xlWorkSheet.Cells[1, 3] = "LOCNCODE";
                xlWorkSheet.Cells[1, 4] = "DOCDATE";
                xlWorkSheet.Cells[1, 5] = "Status";

               /*save as xls file works – it creates new worksheet, give a name of the new worksheet, and save changes
                workbook.SaveAs(file_name, Excel.XlFileFormat.xlWorkbookNormal, System.Reflection.Missing.Value, System.Reflection.Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlShared, false, false, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value);
                Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlShared; mode stops user from changing 
                Excel.XlFileFormat.xlWorkbookNormal; Specifies the file format when saving the spreadsheet. If you do not choose the right format you will see error while opening the file in windows.
                */
                xlWorkBook.SaveAs(file_name, Excel.XlFileFormat.xlWorkbookNormal, System.Reflection.Missing.Value, System.Reflection.Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlExclusive, false, false, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value);
                xlWorkBook.Close();
                xlApp.Quit();
                GC.WaitForPendingFinalizers();

                /*Release the resources. If you do not release the resources properly.
                 File will be still opened by the process and it stops the user from modifying and even from opening*/
                Commons.releaseObject(xlWorkSheet);
                Commons.releaseObject(xlWorkBook);
                Commons.releaseObject(xlApp);
                sheets = null;
                GC.Collect();

                MessageBox.Show("Today's new allocations were saved to " + file_name + ".");
            }

            else // create new file)  
            {
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                
                int i = 0;
                int j = 0;

                for (i = 0; i <= dataGridView1.RowCount - 2; i++)
                {
                     for (j = 1; j <= 5; j++)
                     {
                    DataGridViewCell cell = dataGridView1[j, i];
                    xlWorkSheet.Cells[i + 2, j] = cell.Value;
                     }
                }
                // input colume names:
                xlWorkSheet.Cells[1, 1] = "BACHNUMB";
                xlWorkSheet.Cells[1, 2] = "DocNumber";
                xlWorkSheet.Cells[1, 3] = "LOCNCODE";
                xlWorkSheet.Cells[1, 4] = "DOCDATE";
                xlWorkSheet.Cells[1, 5] = "Status";

                xlWorkSheet.Name = strSheetName;

                xlWorkBook.SaveAs(file_name, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                Commons.releaseObject(xlWorkSheet);
                Commons.releaseObject(xlWorkBook);
                Commons.releaseObject(xlApp);

                MessageBox.Show("Excel file created , you can find the file in " + file_name);
            }
        }
    }
}

