﻿namespace AzuraAppDevUtility
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.nextGenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addDocsToAutoFaxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addWristbandPrinterIPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.migrateReferMktPlansToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taskWorkgroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greatPlaintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gPDeletedNoTransBatchesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlyGPUpdateToRcptsMissingFacilityIDToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.gPAllocationCleanUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gPAccellosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cleanBatchFromAutopostToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nextGenToolStripMenuItem,
            this.greatPlaintToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(6, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(1200, 35);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // nextGenToolStripMenuItem
            // 
            this.nextGenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addDocsToAutoFaxToolStripMenuItem,
            this.addWristbandPrinterIPToolStripMenuItem,
            this.migrateReferMktPlansToolStripMenuItem,
            this.taskWorkgroupToolStripMenuItem});
            this.nextGenToolStripMenuItem.Name = "nextGenToolStripMenuItem";
            this.nextGenToolStripMenuItem.Size = new System.Drawing.Size(95, 29);
            this.nextGenToolStripMenuItem.Text = "NextGen";
            // 
            // addDocsToAutoFaxToolStripMenuItem
            // 
            this.addDocsToAutoFaxToolStripMenuItem.Name = "addDocsToAutoFaxToolStripMenuItem";
            this.addDocsToAutoFaxToolStripMenuItem.Size = new System.Drawing.Size(311, 34);
            this.addDocsToAutoFaxToolStripMenuItem.Text = "Add Docs to AutoFax";
            this.addDocsToAutoFaxToolStripMenuItem.Click += new System.EventHandler(this.addDocsToAutoFaxToolStripMenuItem_Click);
            // 
            // addWristbandPrinterIPToolStripMenuItem
            // 
            this.addWristbandPrinterIPToolStripMenuItem.Name = "addWristbandPrinterIPToolStripMenuItem";
            this.addWristbandPrinterIPToolStripMenuItem.Size = new System.Drawing.Size(311, 34);
            this.addWristbandPrinterIPToolStripMenuItem.Text = "Add Wristband Printer IP";
            this.addWristbandPrinterIPToolStripMenuItem.Click += new System.EventHandler(this.addNG_FVC_PrinterConfig_Click);
            // 
            // migrateReferMktPlansToolStripMenuItem
            // 
            this.migrateReferMktPlansToolStripMenuItem.Name = "migrateReferMktPlansToolStripMenuItem";
            this.migrateReferMktPlansToolStripMenuItem.Size = new System.Drawing.Size(311, 34);
            this.migrateReferMktPlansToolStripMenuItem.Text = "Migrate Refer Mkt Plans";
            this.migrateReferMktPlansToolStripMenuItem.Click += new System.EventHandler(this.MigrateReferMktPlans_Click);
            // 
            // taskWorkgroupToolStripMenuItem
            // 
            this.taskWorkgroupToolStripMenuItem.Name = "taskWorkgroupToolStripMenuItem";
            this.taskWorkgroupToolStripMenuItem.Size = new System.Drawing.Size(311, 34);
            this.taskWorkgroupToolStripMenuItem.Text = "Task Workgroup Sync";
            this.taskWorkgroupToolStripMenuItem.Click += new System.EventHandler(this.taskWorkgroupToolStripMenuItem_Click);
            // 
            // greatPlaintToolStripMenuItem
            // 
            this.greatPlaintToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gPDeletedNoTransBatchesToolStripMenuItem,
            this.monthlyGPUpdateToRcptsMissingFacilityIDToolStripMenuItem1,
            this.gPAllocationCleanUpToolStripMenuItem,
            this.gPAccellosToolStripMenuItem,
            this.cleanBatchFromAutopostToolStripMenuItem});
            this.greatPlaintToolStripMenuItem.Name = "greatPlaintToolStripMenuItem";
            this.greatPlaintToolStripMenuItem.Size = new System.Drawing.Size(120, 29);
            this.greatPlaintToolStripMenuItem.Text = "Great Plains";
            // 
            // gPDeletedNoTransBatchesToolStripMenuItem
            // 
            this.gPDeletedNoTransBatchesToolStripMenuItem.Name = "gPDeletedNoTransBatchesToolStripMenuItem";
            this.gPDeletedNoTransBatchesToolStripMenuItem.Size = new System.Drawing.Size(486, 34);
            this.gPDeletedNoTransBatchesToolStripMenuItem.Text = "GP  Deleted No Trans Batches";
            this.gPDeletedNoTransBatchesToolStripMenuItem.Click += new System.EventHandler(this.CleanNoTrans_Click);
            // 
            // monthlyGPUpdateToRcptsMissingFacilityIDToolStripMenuItem1
            // 
            this.monthlyGPUpdateToRcptsMissingFacilityIDToolStripMenuItem1.Name = "monthlyGPUpdateToRcptsMissingFacilityIDToolStripMenuItem1";
            this.monthlyGPUpdateToRcptsMissingFacilityIDToolStripMenuItem1.Size = new System.Drawing.Size(486, 34);
            this.monthlyGPUpdateToRcptsMissingFacilityIDToolStripMenuItem1.Text = "Monthly GP Update to Rcpts Missing Facility ID";
            this.monthlyGPUpdateToRcptsMissingFacilityIDToolStripMenuItem1.Click += new System.EventHandler(this.Month_GP_No_Receps_Click);
            // 
            // gPAllocationCleanUpToolStripMenuItem
            // 
            this.gPAllocationCleanUpToolStripMenuItem.Name = "gPAllocationCleanUpToolStripMenuItem";
            this.gPAllocationCleanUpToolStripMenuItem.Size = new System.Drawing.Size(486, 34);
            this.gPAllocationCleanUpToolStripMenuItem.Text = "GP Allocation Clean Up";
            this.gPAllocationCleanUpToolStripMenuItem.Click += new System.EventHandler(this.GP_Allocation_CleanUp_Click);
            // 
            // gPAccellosToolStripMenuItem
            // 
            this.gPAccellosToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gPAccellosToolStripMenuItem.Name = "gPAccellosToolStripMenuItem";
            this.gPAccellosToolStripMenuItem.Size = new System.Drawing.Size(486, 34);
            this.gPAccellosToolStripMenuItem.Text = "GP Allocation Step 2";
            this.gPAccellosToolStripMenuItem.Click += new System.EventHandler(this.Accello_Click);
            // 
            // cleanBatchFromAutopostToolStripMenuItem
            // 
            this.cleanBatchFromAutopostToolStripMenuItem.Name = "cleanBatchFromAutopostToolStripMenuItem";
            this.cleanBatchFromAutopostToolStripMenuItem.Size = new System.Drawing.Size(486, 34);
            this.cleanBatchFromAutopostToolStripMenuItem.Text = "Clear Batch from Autopost";
            this.cleanBatchFromAutopostToolStripMenuItem.Click += new System.EventHandler(this.cleanBatchFromAutopostToolStripMenuItem_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 692);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmMain";
            this.Text = "App Dev Utility";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem nextGenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addDocsToAutoFaxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greatPlaintToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gPDeletedNoTransBatchesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monthlyGPUpdateToRcptsMissingFacilityIDToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem addWristbandPrinterIPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem migrateReferMktPlansToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gPAllocationCleanUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gPAccellosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cleanBatchFromAutopostToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taskWorkgroupToolStripMenuItem;
    }
}