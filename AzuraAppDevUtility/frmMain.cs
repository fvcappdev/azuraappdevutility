﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace AzuraAppDevUtility
{
    public partial class frmMain : Form
    {
       
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            Commons.real_user_name= Environment.UserName.ToString();
            //MessageBox.Show("real_user=" + real_user_name);

            // get service account username password for NG
            string userName = ConfigurationManager.AppSettings.Get("service_account_userName");
            string password = ConfigurationManager.AppSettings.Get("service_account_password");

            // Impersonate user
            ImpersonationDemo.Impersonation(userName, password);


        }

        private void addDocsToAutoFaxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new frmAddDocToAutoFax();
            f.ShowDialog();
        }

        private void CleanNoTrans_Click(object sender, EventArgs e)
        {
           var f1 = new frmCleanNoTrans();
             f1.ShowDialog();
        }

        private void Month_GP_No_Receps_Click(object sender, EventArgs e)
        {
            var f3 = new frmAddGPMissingRecp();
            f3.ShowDialog();
        }

        private void addNG_FVC_PrinterConfig_Click(object sender, EventArgs e)
        {
            var f4 = new frmPrinterConfig();
            f4.ShowDialog();
        }

        private void MigrateReferMktPlans_Click(object sender, EventArgs e)
        {
            var f5 = new frmMigrateReferMktPlans();
            f5.ShowDialog();
        }

        private void GP_Allocation_CleanUp_Click(object sender, EventArgs e)
        {
            var f6 = new frmGPAllocationCleanUp();
            f6.ShowDialog();
        }

        private void New_Practice_Click(object sender, EventArgs e)
        {
            var f7 = new frmNewPractice();
            f7.ShowDialog();
        }

        private void Accello_Click(object sender, EventArgs e)
        {
            var f8 = new frmAllocationStep2();
            f8.ShowDialog();
        }

        private void cleanBatchFromAutopostToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var f9 = new frmCleanBatchFromAutpost();
            f9.ShowDialog();
        }

       
        private void taskWorkgroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f11 = new frmTaskWorkgroup();
            f11.ShowDialog();
        }
    }
} 
