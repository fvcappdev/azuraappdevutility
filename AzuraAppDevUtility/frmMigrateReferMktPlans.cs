﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//New
using AzuraAppDevUtility;
using System.Data.SqlClient;
using System.Configuration;

namespace AzuraAppDevUtility
{
    public partial class frmMigrateReferMktPlans : Form
    {
        public frmMigrateReferMktPlans()
        {
            InitializeComponent();
        }

        private void MigrateReferMktPlans_Load(object sender, EventArgs e)
        {
            //set up datagridview with named columns
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "practice_id";
            column.Name = "practice_id";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "mstr_file_id";
            column.Name = "mstr_file_id";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "mstr_file_type";
            column.Name = "mstr_file_type";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "create_timestamp";
            column.Name = "create_timestamp";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "created_by";
            column.Name = "created_by";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "modify_timestamp";
            column.Name = "modify_timestamp";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "modified_by";
            column.Name = "modified_by";
            dataGridView1.Columns.Add(column);


            //
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "practice_id";
            column.Name = "practice_id";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "plan_id";
            column.Name = "plan_id";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "mrkt_plan";
            column.Name = "mrkt_plan";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "description";
            column.Name = "description";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "plan_type";
            column.Name = "plan_type";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "note";
            column.Name = "note";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "delete_ind";
            column.Name = "delete_ind";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "create_timestamp";
            column.Name = "create_timestamp";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "created_by";
            column.Name = "created_by";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "modify_timestamp";
            column.Name = "modify_timestamp";
            dataGridView2.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "modified_by";
            column.Name = "modified_by";
            dataGridView2.Columns.Add(column);

            column = null;

            string str = @"SELECT DISTINCT practice_id,
                                practice_id + '-' + practice_name AS PracticeWithNbr
                           FROM practice
                           WHERE delete_ind <> 'Y'
                                AND practice_id NOT IN ('0001', '0058')
                           order by practice_id";
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    CommandText = str,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                this.cmbFromPractice.DataSource = null;
                this.cmbFromPractice.Items.Clear();
                this.cmbToPractice.DataSource = null;
                this.cmbToPractice.Items.Clear();
                Dictionary<string, string> dataSourceP = new Dictionary<string, string>();
                while (reader.Read())
                {
                    string value = (string)reader["practice_id"];
                    string key = (string)reader["PracticeWithNbr"];
                    dataSourceP.Add(key, value);
                }
                connection.Close();
                this.cmbFromPractice.DisplayMember = "Key";
                this.cmbFromPractice.ValueMember = "Value";
                this.cmbFromPractice.DataSource = new BindingSource(dataSourceP, null);
                this.cmbToPractice.DisplayMember = "Key";
                this.cmbToPractice.ValueMember = "Value";
                this.cmbToPractice.DataSource = new BindingSource(dataSourceP, null);
                //this.cmbToPractice.SelectedIndex++;     //Don't have both from & to combo boxers pointing to same practice..
                this.cmbFromPractice.SelectedItem = null;
                this.cmbToPractice.SelectedItem = null;
            }

        }

        private void RefreshDataGrid()
        {
            string queryString = string.Format(@"SELECT
                                                    [practice_id],
                                                    mstr_file_id,
                                                    mstr_file_type,
                                                    [create_timestamp],
                                                    [created_by],
                                                    [modify_timestamp],
                                                    [modified_by]
                                                FROM practice_mstr_files PMF (NOLOCK)
                                                WHERE PMF.mstr_file_type = 'PR'
                                                AND PMF.practice_id = {0}

                                            SELECT 
	                                            [practice_id],
	                                            plan_id, 
	                                            mrkt_plan,
	                                            description,
	                                            plan_type, 
	                                            note,
	                                            delete_Ind,
	                                            [create_timestamp],
	                                            [created_by],
	                                            [modify_timestamp],
	                                            [modified_by]
                                            FROM marketing_mstr MM (NOLOCK)
                                            WHERE MM.practice_id = {1}",
                                            this.cmbFromPractice.SelectedValue,
                                            this.cmbFromPractice.SelectedValue);
            
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                SqlDataAdapter adapt = new SqlDataAdapter(command);

                DataSet ds = new DataSet();
                adapt.Fill(ds);

                dataGridView1.DataSource = ds.Tables[0];
                dataGridView2.DataSource = ds.Tables[1];


                foreach (DataGridViewRow row in dataGridView1.Rows)
                    row.HeaderCell.Value = (row.Index + 1).ToString();

                foreach (DataGridViewRow row2 in dataGridView2.Rows)
                    row2.HeaderCell.Value = (row2.Index + 1).ToString();

                // user can't make selction 
                dataGridView1.ReadOnly = true;
                dataGridView2.ReadOnly = true;
                dataGridView1.ClearSelection();
                dataGridView2.ClearSelection();
            }
        }

        private void ClearDataGrid()
        {
            string queryString = @"SELECT
                                    [practice_id],
                                    mstr_file_id,
                                    mstr_file_type,
                                    [create_timestamp],
                                    [created_by],
                                    [modify_timestamp],
                                    [modified_by]
                                FROM practice_mstr_files PMF (NOLOCK)
                                WHERE PMF.mstr_file_type = 'PR'
                                AND PMF.practice_id = '9999'

                            SELECT 
	                            [practice_id],
	                            plan_id, 
	                            mrkt_plan,
	                            description,
	                            plan_type, 
	                            note,
	                            delete_Ind,
	                            [create_timestamp],
	                            [created_by],
	                            [modify_timestamp],
	                            [modified_by]
                            FROM marketing_mstr MM (NOLOCK)
                            WHERE MM.practice_id = '9999'";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                SqlDataAdapter adapt = new SqlDataAdapter(command);

                DataSet ds = new DataSet();
                adapt.Fill(ds);

                dataGridView1.DataSource = ds.Tables[0];
                dataGridView2.DataSource = ds.Tables[1];


                foreach (DataGridViewRow row in dataGridView1.Rows)
                    row.HeaderCell.Value = (row.Index + 1).ToString();

                foreach (DataGridViewRow row2 in dataGridView2.Rows)
                    row2.HeaderCell.Value = (row2.Index + 1).ToString();

                // user can't make selction 
                dataGridView1.ReadOnly = true;
                dataGridView2.ReadOnly = true;
                dataGridView1.ClearSelection();
                dataGridView2.ClearSelection();
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (this.cmbFromPractice.SelectedValue == this.cmbToPractice.SelectedValue)
            {
                MessageBox.Show("The From & To practices must be different!", "Practice Entry Error", MessageBoxButtons.OK);
            }
            else if (this.cmbFromPractice.SelectedValue != null && this.cmbToPractice.SelectedValue != null)
            {
                int numberOfRecords;
                string str1 = string.Format(@"INSERT dbo.practice_mstr_files 
                                                (
	                                                practice_id, mstr_file_id, mstr_file_type, create_timestamp, created_by, modify_timestamp, modified_by
                                                )
                                                SELECT 
	                                                '{0}' AS practice_id,
	                                                mstr_file_id, 
	                                                mstr_file_type,
	                                                GETDATE() AS [create_timestamp],
	                                                0 AS [created_by],
	                                                GETDATE() AS [modify_timestamp],
	                                                0 AS [modified_by]
                                                FROM dbo.practice_mstr_files PMF (NOLOCK)
                                                WHERE PMF.mstr_file_type = 'PR'
                                                    AND PMF.practice_id = '{1}'
                                                    AND NOT EXISTS
                                                    (
	                                                    SELECT 1 FROM dbo.practice_mstr_files X (NOLOCK) WHERE X.mstr_file_type = 'PR' AND X.mstr_file_id = PMF.mstr_file_id AND X.practice_id = '{2}'
                                                    )",
                                                this.cmbToPractice.SelectedValue,   //Need to use single quotes around practice_id to ensure leading zeroes don't get dropped upon insert....
                                                this.cmbFromPractice.SelectedValue,
                                                this.cmbToPractice.SelectedValue);
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
                {
                    SqlCommand command = new SqlCommand
                    {
                        CommandText = str1,
                        CommandType = CommandType.Text,
                        Connection = connection
                    };
                    connection.Open();
                    numberOfRecords = command.ExecuteNonQuery();
                    connection.Close();
                }

                //string str22 = string.Format(@"Practice Mstr Files copied to: {0}", this.cmbToPractice.SelectedItem);
                string str22 = string.Format(@"{0} Practice Mstr Files copied to: {1} {2}", numberOfRecords/2, this.cmbToPractice.Text, this.cmbToPractice.SelectedValue);    // Divide by 2 because insert to practice_mstr table calls trigger that inserts N2 *_ar table as well..
                MessageBox.Show(str22, "Practice Mstr Copy", MessageBoxButtons.OK);                                                                                           //ExecuteNonQuery returns double the rows in this situ..

                int numberOfRecords2;
                string str2 = string.Format(@"INSERT dbo.marketing_mstr 
                                                (
	                                                practice_id, plan_id, mrkt_plan, description, plan_type, note, delete_ind, create_timestamp, created_by, modify_timestamp, modified_by
                                                )
                                                SELECT 
	                                                '{0}' AS [practice_id],
	                                                NEWID() AS plan_id, 
	                                                mrkt_plan,
	                                                description,
	                                                plan_type, 
	                                                note,
	                                                delete_ind,
	                                                GETDATE() AS [create_timestamp],
	                                                0 AS [created_by],
	                                                GETDATE() AS [modify_timestamp],
	                                                0 AS [modified_by]
                                                FROM dbo.marketing_mstr MM (NOLOCK)
                                                WHERE MM.practice_id = '{1}'
                                                --AND MM.delete_ind <> 'Y'  -- Optional: can bring these over for historical purposes
                                                AND NOT EXISTS
                                                (
	                                                SELECT 1 FROM marketing_mstr X (NOLOCK) WHERE X.description = MM.description AND X.practice_id = '{2}'
                                                )",
                                                this.cmbToPractice.SelectedValue,   //Need to use single quotes around practice_id to ensure leading zeroes don't get dropped upon insert....
                                                this.cmbFromPractice.SelectedValue,
                                                this.cmbToPractice.SelectedValue);
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
                {
                    SqlCommand command = new SqlCommand
                    {
                        CommandText = str2,
                        CommandType = CommandType.Text,
                        Connection = connection
                    };
                    connection.Open();
                    numberOfRecords2 = command.ExecuteNonQuery();
                    connection.Close();
                }

                string str23 = string.Format(@"{0} Marketing Mstr Files copied to: {1} {2}", numberOfRecords2, this.cmbToPractice.Text, this.cmbToPractice.SelectedValue);
                MessageBox.Show(str23, "Practice Mstr Copy", MessageBoxButtons.OK);
                string str3 = this.cmbFromPractice.SelectedValue.ToString();
                Commons.LogUser2(this.Name, "MigrateReferMktPlans", "", "", "", str3, str22, str23);    //Sizes:  note1 250, note2 500, note3 1000

                ClearDataGrid();
                this.cmbFromPractice.SelectedIndex = -1;    //Clear SelectedValue to NULL.
                this.cmbToPractice.SelectedIndex = -1;
                //this.cmbFromPractice.ResetText();     //Also works, but doesn't clear SelectedValue to null (like SelectedIndex = -1) for else if check above..
                //this.cmbToPractice.ResetText();

            }
            else
            {
                MessageBox.Show("Please select a From and To practice", "Practice Entry Issue", MessageBoxButtons.OK);
            }
            //    int num = 0;
            //    if (this.btnSubmit.Text == "Edit")
            //    {
            //        num = 1;
            //        this.txtFromPractice.ReadOnly = false;
            //        this.txtToPractice.ReadOnly = false;
            //        this.mcLocationDate.Enabled = true;
            //    }
            //    else if (this.btnSubmit.Text == "Submit")
            //    {
            //        num = 2;
            //        this.txtFromPractice.ReadOnly = true;
            //        this.txtToPractice.ReadOnly = true;
            //        this.mcLocationDate.Enabled = false;
            //    }
            //    else
            //    {
            //        MessageBox.Show("I have no clue how you got here......", "Button Errored", MessageBoxButtons.OK);
            //    }
            //    switch (num)
            //    {
            //        case 1:
            //            this.btnSubmit.Text = "Submit";
            //            return;

            //        case 2:
            //            {

            //                string str = "From Pract: " + this.txtFromPractice.ToString() + " To Pract: " + this.txtToPractice.ToString() + " Loc Date: " + this.mcLocationDate.ToString();
            //                string str1 = this.cmbPractice.SelectedText.ToString();     //Likely used to be populated w/ previous version of the form..
            //                string str5;
            //                if (this.txtFromPractice.ToString() == "")
            //                {
            //                    this.txtFromPractice.ReadOnly = false;???
            //                    this.txtToPractice.ReadOnly = false;
            //                    this.mcLocationDate.Enabled = true;
            //                    MessageBox.Show("IP Address and Port Information Saved To Database", "Update Complete", MessageBoxButtons.OK);
            //                }
            //                else if (this.txtToPractice.ToString() == "")
            //                {

            //                }
            //                else if (this.mcLocationDate.ToString() == "")
            //                {

            //                }
            //                if (practice_exist(str3, str4))
            //                {
            //                    str5 = string.Format("UPDATE FVC_wristband_printer_config SET ip_address = '{0}', ip_port = '{1}' WHERE practice_id = '{2}' AND location_id = '{3}'", new object[] { str, str2, str3, str4 });
            //                }
            //                else
            //                {
            //                    //str5 = string.Format("INSERT INTO [NGprod].[dbo].[FVC_wristband_printer_config] ([enterprise_id],[practice_id],[location_id],[ip_address],[ip_port],[printer_name],[created_ts],[last_modified_ts]) VALUES " + "('00001', '{0}', '{1}', '{2}', '{3}', '{4}',GETDATE(),GETDATE())", new object[] { str3, str4, str, str2, str1 });
            //                    str5 = string.Format("INSERT INTO FVC_wristband_printer_config ([enterprise_id],[practice_id],[location_id],[ip_address],[ip_port],[printer_name],[created_ts],[last_modified_ts]) VALUES " + "('00001', '{0}', '{1}', '{2}', '{3}', '{4}',GETDATE(),GETDATE())", new object[] { str3, str4, str, str2, str1 });
            //                }
            //                //SqlConnection connection = new SqlConnection("Data Source=AACEMRVRSQL; Integrated Security=SSPI; Initial Catalog=NGProd");
            //                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
            //                {
            //                    SqlCommand command = new SqlCommand
            //                    {
            //                        CommandText = str5,
            //                        CommandType = CommandType.Text,
            //                        Connection = connection
            //                    };
            //                    connection.Open();
            //                    command.ExecuteNonQuery();
            //                    connection.Close();
            //                }
            //                MessageBox.Show("IP Address and Port Information Saved To Database", "Update Complete", MessageBoxButtons.OK);
            //                this.btnSubmit.Text = "Edit";
            //                //Commons.LogUser(this.Name, "Add Printer IP to FVC_wristband_printer_config", str6, str3, str4);
            //                Commons.LogUser2(this.Name, "Add Printer IP to FVC_wristband_printer_config", "", "", "", str, "", "");
            //                return;
            //            }
            //    }
            //    MessageBox.Show("I have no clue how you got here......", "Update Errored", MessageBoxButtons.OK);
        }

        //private void cmbFromPractice_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}

        private void cmbFromPractice_SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.cmbFromPractice.SelectedValue != null)
            {
                RefreshDataGrid();
            }
        }

        private void cmbFromPractice_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        //private void cmbFromPractice_SelectedValueChanged(object sender, EventArgs e)
        //{
        //    if (this.cmbFromPractice.SelectedValue != null)
        //    {
        //        RefreshDataGrid();
        //    }
        //}
    }
}
