﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;

namespace AzuraAppDevUtility
{
    public partial class frmNewPractice : Form
    {
        public frmNewPractice()
        {
            InitializeComponent();

        }


        private void btnRetrieve_Click(object sender, EventArgs e)
        {
            //Dynamically create SQL Query
            string sqlstmt;
            string centercode;
            centercode = txtCntrCode.Text.ToString();
           // sqlstmt = @"SELECT [locServiceFacID] [ImgCode], [locName] [LegalName], [locAddress1] [Addr1], [locAddress2] [Addr2], [locCity] [City], [locState] [State], [locZip] [Zip], [locNPI] [NPI], [locTaxID] [TaxID] FROM [GR-IMAGINE-SQL\IMAGINE1].[AAC].[dbo].[Locations] WHERE [locAbbreviation] ='" + centercode + "'";

            sqlstmt = @"SELECT [locServiceFacID] [ImgCode], [locName] [LegalName], [locAddress1] [Addr1], [locAddress2] [Addr2], [locCity] [City], [locState] [State], [locZip] [Zip], [locNPI] [NPI], [locTaxID] [TaxID] FROM [dbo].[Locations] WHERE [locAbbreviation] ='" + centercode + "'";

            //String Variables For TxtBox Output
            string ImgCode ="";
            string LglName="";
            string Addr1="";
            string Addr2="";
            string City="";
            string State="";
            string Zip="";
            string NPI=""; 
            string TaxID="";

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ImagingConnString"].ConnectionString))
            {
               
                // SqlConnection conn = new SqlConnection("Server=AACGRDWH1\\MSSQLSERVER1;Integrated Security=SSPI;Initial Catalog=AAC_DWH"); ;
                //Prepare SQL Command            
                SqlCommand cmd = new SqlCommand(sqlstmt, conn);
                cmd.CommandType = CommandType.Text;

                //Open SQL Connection
                conn.Open();

                //Declare SQL Reader
                SqlDataReader rdr = null;

                //Execute SQL Reader
                rdr = cmd.ExecuteReader();

                //Assign Values to String Variables from SQL Reader
                while (rdr.Read())
                {
                    ImgCode = rdr["ImgCode"].ToString();
                    LglName = rdr["LegalName"].ToString();
                    Addr1 = rdr["Addr1"].ToString();
                    Addr2 = rdr["Addr2"].ToString();
                    City = rdr["City"].ToString();
                    State = rdr["State"].ToString();
                    Zip = rdr["Zip"].ToString();
                    NPI = rdr["NPI"].ToString();
                    TaxID = rdr["TaxID"].ToString();
                }
                
            }

            //Format Text
            LglName = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(LglName.ToLower());
            Addr1 = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Addr1.ToLower());
            Addr2 = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Addr2.ToLower());
            City = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(City.ToLower());
            Zip = Zip.Replace("-", "");
            
            //Assign Variables To TextBoxes
            txtImgCode.Text = ImgCode;
            txtLglName.Text = LglName; //limit of 40
            txtAddr1.Text = Addr1; //limit of 55
            txtAddr2.Text = Addr2; //limit of 55
            txtCity.Text = City;
            txtState.Text = State;
            txtZip.Text = Zip; //limit of 9
            txtNPI.Text = NPI;
            txtTaxID.Text = TaxID;
            txtPhone.ReadOnly = false;
            txtFax.ReadOnly = false;

            //Make Modify Center Button Visible
            btnModify.Visible = true;
            btnDemo.Visible = true;
            btnTest.Visible = true;
            btnDev.Visible = true;
            btnProd.Visible = true;
            groupBox1.Visible = true;   
        }

        // added by Patricia
        private void RetriveDataFromNG(String system, String conStringName)
        {
            //string locationID;
            string practiceID;

            if (chkASC.Checked.Equals(false))
            {
                //locationID = "A3B60449-96B2-43BC-A93D-8CB53D26FD81";
                practiceID = "0003";
            }
            else
            {
                //locationID = "666D5DF4-41C1-48FE-9049-57386365D0C1";
                practiceID = "0071";
            }
            
            
            string Group_list = ConfigurationManager.AppSettings.Get("NG_New_practice_Security_Group");
            MessageBox.Show(" Group_list=" + Group_list);

            string admin = "37";
            string appadmin = "38";
            string billing = "39";
            string billingsup = "40";
            string ehrspec = "327";
            string fda = "42";
            string fdss = "43";
            string hd = "325";
            string NA = "326";
            string SQLtrgtIDStmt = @"select l.location_id
                                    from [dbo].practice p
                                    inner join  [dbo].practice_location_xref x
                                    on x.practice_id = p.practice_id
                                    inner join [dbo].location_mstr l
                                    on l.location_id = x.location_id
                                    where claim_practice_id = location_type
                                    and p.practice_id =@practiceID

                                    SELECT CONVERT(char(4), RIGHT('0000'+CONVERT(varchar(4),(MAX(practice_id)+1)),4)) as [PractID] from practice;
                                    SELECT CONVERT(varchar(36),NEWID()) as [LocID]

                                    SELECT (CONVERT(int,MAX(group_id)) + 1) as [TgtID] FROM security_groups WITH (NOLOCK)

                                    SELECT group_id, group_name FROM security_groups WITH (NOLOCK)
                                    where practice_id=@practiceID
                                    and group_name in ('Administrators','Application Admin','Front Desk/Sched /Med Rec Mgr','Help Desk')";

                                    
                                    
                
            int TgtID0 = 0;
            int TgtID1;
            int TgtID2;
            int TgtID3;
            int TgtID4;
            int TgtID5;
            int TgtID6;
            int TgtID7;
            int TgtID8;
            int TgtID9;
            string TPract = "";
            string TLocation = "";
            //string SQLPractStmt = "SELECT CONVERT(char(4), RIGHT('0000'+CONVERT(varchar(4),(MAX(practice_id)+1)),4)) as [PractID] from practice";
            //string SQLLocStmt = "SELECT CONVERT(varchar(36),NEWID()) as [LocID]";
            txtAdmin.Text = admin;
            txtAppAdmin.Text = appadmin;
            txtBilling.Text = billing;
            txtBillingSup.Text = billingsup;
            txtEHR.Text = ehrspec;
            txtFDA.Text = fda;
            txtFDSS.Text = fdss;
            txtHD.Text = hd;
            txtNA.Text = NA;
            txtfpract.Text = practiceID;
            //txtfloc.Text = locationID;
            txtSystem.Text = system;

            string locationID=null;
            SqlCommand cmd;
            SqlDataReader rdr = null;
            //Create SQL Connection Variable
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NGTESTConnString"].ConnectionString))
           {
                // SqlConnection conn = new SqlConnection("Server=TS-NEXGN-DB-02;Integrated Security=SSPI;Initial Catalog=NGDemo"); ;
                //Prepare SQL Command            
                cmd = new SqlCommand(SQLtrgtIDStmt, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@practiceID", practiceID);
                //Open SQL Connection
                conn.Open();
                //Declare SQL Reader
                
                //Execute SQL Reader
                rdr = cmd.ExecuteReader();

                //1.Assign  003 or 0071 practice location_id Values to String Variables from SQL Reader
                while (rdr.Read())
                {
                    locationID = (rdr["Location_id"].ToString().ToUpper());
                }

                txtfloc.Text = locationID;
                MessageBox.Show(" locationID=" + locationID);

                // 2.assign practice_id
                rdr.NextResult();
                while (rdr.Read())
                {
                    TPract = rdr["PractID"].ToString();
                }
                txtTPract.Text = TPract;

                //MessageBox.Show(" TPract=" + TPract);

                //3. assign max location id for new location
                rdr.NextResult();
                while (rdr.Read())
                {
                    TLocation = rdr["LocID"].ToString();
                }

                txtTloc.Text = TLocation;
                btnAutomate.Visible = true;

                MessageBox.Show(" TLocation=" + TLocation);


                //4.Assign new Values to String Variables TgtID0 from SQL Reader
                rdr.NextResult();
                while (rdr.Read())
                {
                    TgtID0 = int.Parse(rdr["TgtID"].ToString());
                }
               
                MessageBox.Show(" TgtID0=" + TgtID0);


                TgtID1 = TgtID0;
                TgtID2 = TgtID0 + 1;
                TgtID3 = TgtID0 + 2;
                TgtID4 = TgtID0 + 3;
                TgtID5 = TgtID0 + 4;
                TgtID6 = TgtID0 + 5;
                TgtID7 = TgtID0 + 6;
                TgtID8 = TgtID0 + 7;
                TgtID9 = TgtID0 + 8;

                txtTadmin.Text = TgtID1.ToString();
                txtTAppAdmin.Text = TgtID2.ToString();
                txtTBilling.Text = TgtID3.ToString();
                txtTBillingSup.Text = TgtID4.ToString();
                txtTEHR.Text = TgtID5.ToString();
                txtTFDA.Text = TgtID6.ToString();
                txtTFDSS.Text = TgtID7.ToString();
                txtTHD.Text = TgtID8.ToString();
                txtTNA.Text = TgtID9.ToString();


                String group_id = null;
                String group_name = null;
                // 5, assign security group
                rdr.NextResult();
                while (rdr.Read())
                {
                    group_id = rdr["group_id"].ToString();
                    group_name = rdr["group_name"].ToString();
                }

                //txtTloc.Text = TLocation;
                //btnAutomate.Visible = true;

                MessageBox.Show(" group_id=" + group_id);
                MessageBox.Show(" group_name=" + group_name);

            }
          
        }

        private void btn_Demo_Click(object sender, EventArgs e)
        {
            String system = "4";
            String constring = "NGTESTConnString";

            RetriveDataFromNG(system, constring);
        }

        private void btnDev_Click(object sender, EventArgs e)
        {
            String system = "1";
            String constring = "NGTESTConnString";

            RetriveDataFromNG(system, constring);
        }

        private void btnProd_Click(object sender, EventArgs e)
        {
            String system = "3";
            String constring = "NGTESTConnString";

            RetriveDataFromNG(system, constring);
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            String system = "2";
            String constring = "NGTESTConnString";

            RetriveDataFromNG(system, constring);
        }

        private void btnAutomate_Click(object sender, EventArgs e)
        {

            Automate(txtSystem.Text);
        }

        private void Automate(String txtSystem)
        {

            String msg = "";
            String Conn = "";
            if (txtSystem == "1")
            {
                msg = "NGDev Automation";
                Conn = "NGTESTConnString";
            }

            else if (txtSystem == "2")
            {
                msg = "NGTEST Automation";
                Conn = "NGTESTConnString";
            }
            else if (txtSystem == "3")
            {
                msg = "NGPROD Automation";
                Conn = "NGTESTConnString";
            }

            else if (txtSystem == "4")
            {
                msg = "NGDemo Automation";
                Conn = "NGTESTConnString";
            }



            string flocID = txtfloc.Text;
            string tlocID = txtTloc.Text;
            string fpracID = txtfpract.Text;
            string tpracID = txtTPract.Text;
            string centcode = txtCntrCode.Text;
            string imgcode = txtImgCode.Text;
            string lglname = txtLglName.Text;
            string addr1 = txtAddr1.Text;
            string addr2 = txtAddr2.Text;
            string zip = txtZip.Text;
            string npi = txtNPI.Text;
            string taxid = txtTaxID.Text;
            string phone = txtPhone.Text;
            string fax = txtFax.Text;
            string asrc = txtAdmin.Text;
            string atgt = txtTadmin.Text;
            string aasrc = txtAppAdmin.Text;
            string aatgt = txtTAppAdmin.Text;
            string bsrc = txtBilling.Text;
            string btgt = txtTBilling.Text;
            string bssrc = txtBillingSup.Text;
            string bstgt = txtTBillingSup.Text;
            string esrc = txtEHR.Text;
            string etgt = txtTEHR.Text;
            string fasrc = txtFDA.Text;
            string fatgt = txtTFDA.Text;
            string fssrc = txtFDSS.Text;
            string fstgt = txtTFDSS.Text;
            string hsrc = txtHD.Text;
            string htgt = txtTHD.Text;
            string nsrc = txtNA.Text;
            string ntgt = txtTNA.Text;

            //SqlConnection conn1 = new SqlConnection("Server=TS-NEXGN-DB-02;Integrated Security=SSPI;Initial Catalog=NGDevl");
            using (SqlConnection conn1 = new SqlConnection(ConfigurationManager.ConnectionStrings[Conn].ConnectionString))
            {
                conn1.Open();
                SqlCommand cmd = new SqlCommand("NG_Copy", conn1);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Flocation_id", flocID));
                cmd.Parameters.Add(new SqlParameter("@Tlocation_id", tlocID));
                cmd.Parameters.Add(new SqlParameter("@Fpractice_ID", fpracID));
                cmd.Parameters.Add(new SqlParameter("@Tpractice_id", tpracID));
                cmd.Parameters.Add(new SqlParameter("@center_code", centcode));
                cmd.Parameters.Add(new SqlParameter("@imagine_code", imgcode));
                cmd.Parameters.Add(new SqlParameter("@legal_name", lglname));
                cmd.Parameters.Add(new SqlParameter("@address1", addr1));
                cmd.Parameters.Add(new SqlParameter("@address2", addr2));
                cmd.Parameters.Add(new SqlParameter("@zipfull", zip));
                cmd.Parameters.Add(new SqlParameter("@NPI", npi));
                cmd.Parameters.Add(new SqlParameter("@TaxID", taxid));
                cmd.Parameters.Add(new SqlParameter("@Phone", phone));
                cmd.Parameters.Add(new SqlParameter("@Fax", fax));
                cmd.Parameters.Add(new SqlParameter("@Asrc_id", asrc));
                cmd.Parameters.Add(new SqlParameter("@Atgt_id", atgt));
                cmd.Parameters.Add(new SqlParameter("@AAsrc_id", aasrc));
                cmd.Parameters.Add(new SqlParameter("@AAtgt_id", aatgt));
                cmd.Parameters.Add(new SqlParameter("@Bsrc_id", bsrc));
                cmd.Parameters.Add(new SqlParameter("@Btgt_id", btgt));
                cmd.Parameters.Add(new SqlParameter("@BSsrc_id", bssrc));
                cmd.Parameters.Add(new SqlParameter("@BStgt_id", bstgt));
                cmd.Parameters.Add(new SqlParameter("@Esrc_id", esrc));
                cmd.Parameters.Add(new SqlParameter("@Etgt_id", etgt));
                cmd.Parameters.Add(new SqlParameter("@FAsrc_id", fasrc));
                cmd.Parameters.Add(new SqlParameter("@FAtgt_id", fatgt));
                cmd.Parameters.Add(new SqlParameter("@FSsrc_id", fssrc));
                cmd.Parameters.Add(new SqlParameter("@FStgt_id", fstgt));
                cmd.Parameters.Add(new SqlParameter("@Hsrc_id", hsrc));
                cmd.Parameters.Add(new SqlParameter("@Htgt_id", htgt));
                cmd.Parameters.Add(new SqlParameter("@Nsrc_id", nsrc));
                cmd.Parameters.Add(new SqlParameter("@Ntgt_id", ntgt));
                cmd.ExecuteNonQuery();
                // conn1.Close();
            }
            MessageBox.Show("All Done", msg, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        private void btnClear_Click(object sender, EventArgs e)
        {
            txtCntrCode.Text = "";
            txtImgCode.Text = "";
            txtLglName.Text = "";
            txtAddr1.Text = "";
            txtAddr2.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            txtZip.Text = "";
            txtNPI.Text = "";
            txtTaxID.Text = "";
            txtTPract.Text = "";
            txtfpract.Text = "";
            txtfloc.Text = "";
            txtTloc.Text = "";
            btnModify.Text = "Modify Data";
            btnAutomate.Visible = false;
            txtPhone.Text = "";
            txtFax.Text = "";
            txtAdmin.Text = "";
            txtAppAdmin.Text = "";
            txtBilling.Text = "";
            txtBillingSup.Text = "";
            txtEHR.Text = "";
            txtFDA.Text = "";
            txtFDSS.Text = "";
            txtTFDSS.Text = "";
            txtTFDA.Text = "";
            txtTEHR.Text = "";
            txtTBillingSup.Text = "";
            txtTBilling.Text = "";
            txtTAppAdmin.Text = "";
            txtTadmin.Text = "";
            txtSystem.Text = "0";
            txtTNA.Text = "";
            txtTHD.Text = "";
            txtNA.Text = "";
            txtHD.Text = "";
            btnModify.Visible = false;
            btnTest.Visible = false;
            btnDev.Visible = false;
            btnDemo.Visible = false;
            btnProd.Visible = false;
            groupBox1.Visible = false;
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            string btnText = btnModify.Text.ToString();

            if (btnText == "Modify Data")
            {
                txtLglName.ReadOnly = false;
                txtAddr1.ReadOnly = false;
                txtAddr2.ReadOnly = false;
                txtCity.ReadOnly = false;
                txtState.ReadOnly = false;
                txtZip.ReadOnly = false;
                txtNPI.ReadOnly = false;
                txtTaxID.ReadOnly = false;
                btnModify.Text = "Save Data";
            }
            else if (btnText == "Save Data")
            {
                txtLglName.ReadOnly = true;
                txtAddr1.ReadOnly = true;
                txtAddr2.ReadOnly = true;
                txtCity.ReadOnly = true;
                txtState.ReadOnly = true;
                txtZip.ReadOnly = true;
                txtNPI.ReadOnly = true;
                txtTaxID.ReadOnly = true;
                btnModify.Text = "Modify Data";
            }
        }

        private void txtLglName_TextChanged(object sender, EventArgs e)
        {
            string x = txtLglName.Text;
            if (x.Length > 40)
            {
                lblLglName.ForeColor = System.Drawing.Color.Blue;
            }
            else
            {
                lblLglName.ForeColor = System.Drawing.Color.Black;
            }
        }

        private void txtAddr1_TextChanged(object sender, EventArgs e)
        {
            string x = txtAddr1.Text;
            if (x.Length > 55)
            {
                lblAddr1.ForeColor = System.Drawing.Color.Blue;
            }
            else
            {
                lblAddr1.ForeColor = System.Drawing.Color.Black;
            }
        }

        private void txtAddr2_TextChanged(object sender, EventArgs e)
        {
            string x = txtAddr2.Text;
            if (x.Length > 55)
            {
                lblAddr2.ForeColor = System.Drawing.Color.Blue;
            }
            else
            {
                lblAddr2.ForeColor = System.Drawing.Color.Black;
            }
        }

        private void txtZip_TextChanged(object sender, EventArgs e)
        {
            string x = txtZip.Text;
            if (x.Length > 10)
            {
                lblZip.ForeColor = System.Drawing.Color.Blue;
            }
            else
            {
                lblZip.ForeColor = System.Drawing.Color.Black;
            }
        }


    }

    /*
     private void btnDev_Click(object sender, EventArgs e)
    { string locationID;
        string practiceID;

        if (chkASC.Checked.Equals(false))
        {

            locationID = "A3B60449-96B2-43BC-A93D-8CB53D26FD81";

            practiceID = "0003";
        }
        else
        {

            locationID = "1094070D-707E-4B17-B2E6-A371054A864B";

            practiceID = "0071";
        }
        string system = "1";
        string admin = "37";
        string appadmin = "38";
        string billing = "39";
        string billingsup = "40";
        string ehrspec = "327";
        string fda = "42";
        string fdss = "43";
        string hd = "325";
        string NA = "326";
        string SQLtrgtIDStmt = "SELECT (CONVERT(int,MAX(group_id)) + 1) as [TgtID] FROM security_groups WITH (NOLOCK)";
        int TgtID0 = 0;
        int TgtID1;
        int TgtID2;
        int TgtID3;
        int TgtID4;
        int TgtID5;
        int TgtID6;
        int TgtID7;
        int TgtID8;
        int TgtID9;
        string TPract = "";
        string TLocation = "";
        string SQLPractStmt = "SELECT CONVERT(char(4), RIGHT('0000'+CONVERT(varchar(4),(MAX(practice_id)+1)),4)) as [PractID] from practice";
        string SQLLocStmt = "SELECT CONVERT(varchar(36),NEWID()) as [LocID]";
        txtAdmin.Text = admin;
        txtAppAdmin.Text = appadmin;
        txtBilling.Text = billing;
        txtBillingSup.Text = billingsup;
        txtEHR.Text = ehrspec;
        txtFDA.Text = fda;
        txtFDSS.Text = fdss;
        txtHD.Text = hd;
        txtNA.Text = NA;
        txtfpract.Text = practiceID;
        txtfloc.Text = locationID;
        txtSystem.Text = system;


        //Create SQL Connection Variable
        SqlConnection conn = new SqlConnection("Server=TS-NEXGN-DB-02;Integrated Security=SSPI;Initial Catalog=NGDevl"); ;
        //Prepare SQL Command            
        SqlCommand cmd = new SqlCommand(SQLtrgtIDStmt, conn);
        cmd.CommandType = CommandType.Text;
        //Open SQL Connection
        conn.Open();
        //Declare SQL Reader
        SqlDataReader rdr = null;
        //Execute SQL Reader
        rdr = cmd.ExecuteReader();
        //Assign Values to String Variables from SQL Reader
        while (rdr.Read())
        {
            TgtID0 = int.Parse(rdr["TgtID"].ToString());
        }
        //Close SQL Connection
        conn.Close();
        TgtID1 = TgtID0;
        TgtID2 = TgtID0 + 1;
        TgtID3 = TgtID0 + 2;
        TgtID4 = TgtID0 + 3;
        TgtID5 = TgtID0 + 4;
        TgtID6 = TgtID0 + 5;
        TgtID7 = TgtID0 + 6;
        TgtID8 = TgtID0 + 7;
        TgtID9 = TgtID0 + 8;

        txtTadmin.Text = TgtID1.ToString();
        txtTAppAdmin.Text = TgtID2.ToString();
        txtTBilling.Text = TgtID3.ToString();
        txtTBillingSup.Text = TgtID4.ToString();
        txtTEHR.Text = TgtID5.ToString();
        txtTFDA.Text = TgtID6.ToString();
        txtTFDSS.Text = TgtID7.ToString();
        txtTHD.Text = TgtID8.ToString();
        txtTNA.Text = TgtID9.ToString();
        cmd = new SqlCommand(SQLPractStmt, conn);
        cmd.CommandType = CommandType.Text;
        //Open SQL Connection
        conn.Open();
        //Execute SQL Reader
        rdr = cmd.ExecuteReader();
        //Assign Values to String Variables from SQL Reader
        while (rdr.Read())
        {
            TPract = rdr["PractID"].ToString();
        }
        //Close SQL Connection
        conn.Close();
        txtTPract.Text = TPract;
        cmd = new SqlCommand(SQLLocStmt, conn);
        cmd.CommandType = CommandType.Text;
        //Open SQL Connection
        conn.Open();
        //Execute SQL Reader
        rdr = cmd.ExecuteReader();
        //Assign Values to String Variables from SQL Reader
        while (rdr.Read())
        {
            TLocation = rdr["LocID"].ToString();
        }
        //Close SQL Connection
        conn.Close();
        txtTloc.Text = TLocation;
        btnAutomate.Visible = true;
    }*/


    /*
      private void btnTest_Click(object sender, EventArgs e)
    {
      string locationID;
     string practiceID;

     if (chkASC.Checked.Equals(false))
     {

         locationID = "A3B60449-96B2-43BC-A93D-8CB53D26FD81";
         practiceID = "0003";
     }
     else
     {

         locationID = "1094070D-707E-4B17-B2E6-A371054A864B";
         practiceID = "0071";
     }
     string system = "2";
     string admin = "37";
     string appadmin = "38";
     string billing = "39";
     string billingsup = "40";
     string ehrspec = "327";
     string fda = "42";
     string fdss = "43";
     string hd = "325";
     string NA = "326";
     string SQLtrgtIDStmt = "SELECT (CONVERT(int,MAX(group_id)) + 1) as [TgtID] FROM security_groups WITH (NOLOCK)";
     int TgtID0 = 0;
     int TgtID1;
     int TgtID2;
     int TgtID3;
     int TgtID4;
     int TgtID5;
     int TgtID6;
     int TgtID7;
     int TgtID8;
     int TgtID9;
     string TPract = "";
     string TLocation = "";
     string SQLPractStmt = "SELECT CONVERT(char(4), RIGHT('0000'+CONVERT(varchar(4),(MAX(practice_id)+1)),4)) as [PractID] from practice";
     string SQLLocStmt = "SELECT CONVERT(varchar(36),NEWID()) as [LocID]";
     txtAdmin.Text = admin;
     txtAppAdmin.Text = appadmin;
     txtBilling.Text = billing;
     txtBillingSup.Text = billingsup;
     txtEHR.Text = ehrspec;
     txtFDA.Text = fda;
     txtFDSS.Text = fdss;
     txtHD.Text = hd;
     txtNA.Text = NA;
     txtfpract.Text = practiceID;
     txtfloc.Text = locationID;
     txtSystem.Text = system;


     //Create SQL Connection Variable
     SqlConnection conn = new SqlConnection("Server=DR-NXGN-DB-TEST;Integrated Security=SSPI;Initial Catalog=NGTest"); ;
     //Prepare SQL Command            
     SqlCommand cmd = new SqlCommand(SQLtrgtIDStmt, conn);
     cmd.CommandType = CommandType.Text;
     //Open SQL Connection
     conn.Open();
     //Declare SQL Reader
     SqlDataReader rdr = null;
     //Execute SQL Reader
     rdr = cmd.ExecuteReader();
     //Assign Values to String Variables from SQL Reader
     while (rdr.Read())
     {
         TgtID0 = int.Parse(rdr["TgtID"].ToString());
     }
     //Close SQL Connection
     conn.Close();
     TgtID1 = TgtID0;
     TgtID2 = TgtID0 + 1;
     TgtID3 = TgtID0 + 2;
     TgtID4 = TgtID0 + 3;
     TgtID5 = TgtID0 + 4;
     TgtID6 = TgtID0 + 5;
     TgtID7 = TgtID0 + 6;
     TgtID8 = TgtID0 + 7;
     TgtID9 = TgtID0 + 8;

     txtTadmin.Text = TgtID1.ToString();
     txtTAppAdmin.Text = TgtID2.ToString();
     txtTBilling.Text = TgtID3.ToString();
     txtTBillingSup.Text = TgtID4.ToString();
     txtTEHR.Text = TgtID5.ToString();
     txtTFDA.Text = TgtID6.ToString();
     txtTFDSS.Text = TgtID7.ToString();
     txtTHD.Text = TgtID8.ToString();
     txtTNA.Text = TgtID9.ToString();
     cmd = new SqlCommand(SQLPractStmt, conn);
     cmd.CommandType = CommandType.Text;
     //Open SQL Connection
     conn.Open();
     //Execute SQL Reader
     rdr = cmd.ExecuteReader();
     //Assign Values to String Variables from SQL Reader
     while (rdr.Read())
     {
         TPract = rdr["PractID"].ToString();
     }
     //Close SQL Connection
     conn.Close();
     txtTPract.Text = TPract;
     cmd = new SqlCommand(SQLLocStmt, conn);
     cmd.CommandType = CommandType.Text;
     //Open SQL Connection
     conn.Open();
     //Execute SQL Reader
     rdr = cmd.ExecuteReader();
     //Assign Values to String Variables from SQL Reader
     while (rdr.Read())
     {
         TLocation = rdr["LocID"].ToString();
     }
     //Close SQL Connection
     conn.Close();
     txtTloc.Text = TLocation;
     btnAutomate.Visible = true;
 }*/



    /*private void btnProd_Click(object sender, EventArgs e)
    {
     * string locationID;
        string practiceID;

        if (chkASC.Checked.Equals(false))
        {

            locationID = "A3B60449-96B2-43BC-A93D-8CB53D26FD81";
            practiceID = "0003";
        }
        else
        {

            locationID = "666D5DF4-41C1-48FE-9049-57386365D0C1";
            practiceID = "0071";
        }
        string system = "3";
        string admin = "37";
        string appadmin = "38";
        string billing = "39";
        string billingsup = "40";
        string ehrspec = "327";
        string fda = "42";
        string fdss = "43";
        string hd = "325";
        string NA = "326";
        string SQLtrgtIDStmt = "SELECT (CONVERT(int,MAX(group_id)) + 1) as [TgtID] FROM security_groups WITH (NOLOCK)";
        int TgtID0 = 0;
        int TgtID1;
        int TgtID2;
        int TgtID3;
        int TgtID4;
        int TgtID5;
        int TgtID6;
        int TgtID7;
        int TgtID8;
        int TgtID9;
        string TPract = "";
        string TLocation = "";
        string SQLPractStmt = "SELECT CONVERT(char(4), RIGHT('0000'+CONVERT(varchar(4),(MAX(practice_id)+1)),4)) as [PractID] from practice";
        string SQLLocStmt = "SELECT CONVERT(varchar(36),NEWID()) as [LocID]";
        txtAdmin.Text = admin;
        txtAppAdmin.Text = appadmin;
        txtBilling.Text = billing;
        txtBillingSup.Text = billingsup;
        txtEHR.Text = ehrspec;
        txtFDA.Text = fda;
        txtFDSS.Text = fdss;
        txtHD.Text = hd;
        txtNA.Text = NA;
        txtfpract.Text = practiceID;
        txtfloc.Text = locationID;
        txtSystem.Text = system;


        //Create SQL Connection Variable
        SqlConnection conn = new SqlConnection("Server=DC-NEXGN-DB-01;Integrated Security=SSPI;Initial Catalog=NGProd"); ;
        //Prepare SQL Command            
        SqlCommand cmd = new SqlCommand(SQLtrgtIDStmt, conn);
        cmd.CommandType = CommandType.Text;
        //Open SQL Connection
        conn.Open();
        //Declare SQL Reader
        SqlDataReader rdr = null;
        //Execute SQL Reader
        rdr = cmd.ExecuteReader();
        //Assign Values to String Variables from SQL Reader
        while (rdr.Read())
        {
            TgtID0 = int.Parse(rdr["TgtID"].ToString());
        }
        //Close SQL Connection
        conn.Close();
        TgtID1 = TgtID0;
        TgtID2 = TgtID0 + 1;
        TgtID3 = TgtID0 + 2;
        TgtID4 = TgtID0 + 3;
        TgtID5 = TgtID0 + 4;
        TgtID6 = TgtID0 + 5;
        TgtID7 = TgtID0 + 6;
        TgtID8 = TgtID0 + 7;
        TgtID9 = TgtID0 + 8;

        txtTadmin.Text = TgtID1.ToString();
        txtTAppAdmin.Text = TgtID2.ToString();
        txtTBilling.Text = TgtID3.ToString();
        txtTBillingSup.Text = TgtID4.ToString();
        txtTEHR.Text = TgtID5.ToString();
        txtTFDA.Text = TgtID6.ToString();
        txtTFDSS.Text = TgtID7.ToString();
        txtTHD.Text = TgtID8.ToString();
        txtTNA.Text = TgtID9.ToString();
        cmd = new SqlCommand(SQLPractStmt, conn);
        cmd.CommandType = CommandType.Text;
        //Open SQL Connection
        conn.Open();
        //Execute SQL Reader
        rdr = cmd.ExecuteReader();
        //Assign Values to String Variables from SQL Reader
        while (rdr.Read())
        {
            TPract = rdr["PractID"].ToString();
        }
        //Close SQL Connection
        conn.Close();
        txtTPract.Text = TPract;
        cmd = new SqlCommand(SQLLocStmt, conn);
        cmd.CommandType = CommandType.Text;
        //Open SQL Connection
        conn.Open();
        //Execute SQL Reader
        rdr = cmd.ExecuteReader();
        //Assign Values to String Variables from SQL Reader
        while (rdr.Read())
        {
            TLocation = rdr["LocID"].ToString();
        }
        //Close SQL Connection
        conn.Close();
        txtTloc.Text = TLocation;
        btnAutomate.Visible = true;
    }*/


    /*
    private void btnAutomate_Click(object sender, EventArgs e)
    { 
    if (txtSystem.Text == "1")
    {
        string flocID = txtfloc.Text;
        string tlocID = txtTloc.Text;
        string fpracID = txtfpract.Text;
        string tpracID = txtTPract.Text;
        string centcode = txtCntrCode.Text;
        string imgcode = txtImgCode.Text;
        string lglname = txtLglName.Text;
        string addr1 = txtAddr1.Text;
        string addr2 = txtAddr2.Text;
        string zip = txtZip.Text;
        string npi = txtNPI.Text;
        string taxid = txtTaxID.Text;
        string phone = txtPhone.Text;
        string fax = txtFax.Text;
        string asrc = txtAdmin.Text;
        string atgt = txtTadmin.Text;
        string aasrc = txtAppAdmin.Text;
        string aatgt = txtTAppAdmin.Text;
        string bsrc = txtBilling.Text;
        string btgt = txtTBilling.Text;
        string bssrc = txtBillingSup.Text;
        string bstgt = txtTBillingSup.Text;
        string esrc = txtEHR.Text;
        string etgt = txtTEHR.Text;
        string fasrc = txtFDA.Text;
        string fatgt = txtTFDA.Text;
        string fssrc = txtFDSS.Text;
        string fstgt = txtTFDSS.Text;
        string hsrc = txtHD.Text;
        string htgt = txtTHD.Text;
        string nsrc = txtNA.Text;
        string ntgt = txtTNA.Text;

        SqlConnection conn1 = new SqlConnection("Server=TS-NEXGN-DB-02;Integrated Security=SSPI;Initial Catalog=NGDevl");
        conn1.Open();
        SqlCommand cmd = new SqlCommand("NG_Copy", conn1);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add(new SqlParameter("@Flocation_id", flocID));
        cmd.Parameters.Add(new SqlParameter("@Tlocation_id", tlocID));
        cmd.Parameters.Add(new SqlParameter("@Fpractice_ID", fpracID));
        cmd.Parameters.Add(new SqlParameter("@Tpractice_id", tpracID));
        cmd.Parameters.Add(new SqlParameter("@center_code", centcode));
        cmd.Parameters.Add(new SqlParameter("@imagine_code", imgcode));
        cmd.Parameters.Add(new SqlParameter("@legal_name", lglname));
        cmd.Parameters.Add(new SqlParameter("@address1", addr1));
        cmd.Parameters.Add(new SqlParameter("@address2", addr2));
        cmd.Parameters.Add(new SqlParameter("@zipfull", zip));
        cmd.Parameters.Add(new SqlParameter("@NPI", npi));
        cmd.Parameters.Add(new SqlParameter("@TaxID", taxid));
        cmd.Parameters.Add(new SqlParameter("@Phone", phone));
        cmd.Parameters.Add(new SqlParameter("@Fax", fax));
        cmd.Parameters.Add(new SqlParameter("@Asrc_id", asrc));
        cmd.Parameters.Add(new SqlParameter("@Atgt_id", atgt));
        cmd.Parameters.Add(new SqlParameter("@AAsrc_id", aasrc));
        cmd.Parameters.Add(new SqlParameter("@AAtgt_id", aatgt));
        cmd.Parameters.Add(new SqlParameter("@Bsrc_id", bsrc));
        cmd.Parameters.Add(new SqlParameter("@Btgt_id", btgt));
        cmd.Parameters.Add(new SqlParameter("@BSsrc_id", bssrc));
        cmd.Parameters.Add(new SqlParameter("@BStgt_id", bstgt));
        cmd.Parameters.Add(new SqlParameter("@Esrc_id", esrc));
        cmd.Parameters.Add(new SqlParameter("@Etgt_id", etgt));
        cmd.Parameters.Add(new SqlParameter("@FAsrc_id", fasrc));
        cmd.Parameters.Add(new SqlParameter("@FAtgt_id", fatgt));
        cmd.Parameters.Add(new SqlParameter("@FSsrc_id", fssrc));
        cmd.Parameters.Add(new SqlParameter("@FStgt_id", fstgt));
        cmd.Parameters.Add(new SqlParameter("@Hsrc_id", hsrc));
        cmd.Parameters.Add(new SqlParameter("@Htgt_id", htgt));
        cmd.Parameters.Add(new SqlParameter("@Nsrc_id", nsrc));
        cmd.Parameters.Add(new SqlParameter("@Ntgt_id", ntgt));
        cmd.ExecuteNonQuery();
        conn1.Close();
        MessageBox.Show("All Done","NGDev Automation",MessageBoxButtons.OK,MessageBoxIcon.Information);
    }
    else if (txtSystem.Text == "2")
    {
        string flocID = txtfloc.Text;
        string tlocID = txtTloc.Text;
        string fpracID = txtfpract.Text;
        string tpracID = txtTPract.Text;
        string centcode = txtCntrCode.Text;
        string imgcode = txtImgCode.Text;
        string lglname = txtLglName.Text;
        string addr1 = txtAddr1.Text;
        string addr2 = txtAddr2.Text;
        string zip = txtZip.Text;
        string npi = txtNPI.Text;
        string taxid = txtTaxID.Text;
        string phone = txtPhone.Text;
        string fax = txtFax.Text;
        string asrc = txtAdmin.Text;
        string atgt = txtTadmin.Text;
        string aasrc = txtAppAdmin.Text;
        string aatgt = txtTAppAdmin.Text;
        string bsrc = txtBilling.Text;
        string btgt = txtTBilling.Text;
        string bssrc = txtBillingSup.Text;
        string bstgt = txtTBillingSup.Text;
        string esrc = txtEHR.Text;
        string etgt = txtTEHR.Text;
        string fasrc = txtFDA.Text;
        string fatgt = txtTFDA.Text;
        string fssrc = txtFDSS.Text;
        string fstgt = txtTFDSS.Text;
        string hsrc = txtHD.Text;
        string htgt = txtTHD.Text;
        string nsrc = txtNA.Text;
        string ntgt = txtTNA.Text;

        SqlConnection conn2 = new SqlConnection("Server=DR-NXGN-DB-TEST;Integrated Security=SSPI;Initial Catalog=NGTest");
        conn2.Open();
        SqlCommand cmd = new SqlCommand("NG_Copy", conn2);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add(new SqlParameter("@Flocation_id", flocID));
        cmd.Parameters.Add(new SqlParameter("@Tlocation_id", tlocID));
        cmd.Parameters.Add(new SqlParameter("@Fpractice_ID", fpracID));
        cmd.Parameters.Add(new SqlParameter("@Tpractice_id", tpracID));
        cmd.Parameters.Add(new SqlParameter("@center_code", centcode));
        cmd.Parameters.Add(new SqlParameter("@imagine_code", imgcode));
        cmd.Parameters.Add(new SqlParameter("@legal_name", lglname));
        cmd.Parameters.Add(new SqlParameter("@address1", addr1));
        cmd.Parameters.Add(new SqlParameter("@address2", addr2));
        cmd.Parameters.Add(new SqlParameter("@zipfull", zip));
        cmd.Parameters.Add(new SqlParameter("@NPI", npi));
        cmd.Parameters.Add(new SqlParameter("@TaxID", taxid));
        cmd.Parameters.Add(new SqlParameter("@Phone", phone));
        cmd.Parameters.Add(new SqlParameter("@Fax", fax));
        cmd.Parameters.Add(new SqlParameter("@Asrc_id", asrc));
        cmd.Parameters.Add(new SqlParameter("@Atgt_id", atgt));
        cmd.Parameters.Add(new SqlParameter("@AAsrc_id", aasrc));
        cmd.Parameters.Add(new SqlParameter("@AAtgt_id", aatgt));
        cmd.Parameters.Add(new SqlParameter("@Bsrc_id", bsrc));
        cmd.Parameters.Add(new SqlParameter("@Btgt_id", btgt));
        cmd.Parameters.Add(new SqlParameter("@BSsrc_id", bssrc));
        cmd.Parameters.Add(new SqlParameter("@BStgt_id", bstgt));
        cmd.Parameters.Add(new SqlParameter("@Esrc_id", esrc));
        cmd.Parameters.Add(new SqlParameter("@Etgt_id", etgt));
        cmd.Parameters.Add(new SqlParameter("@FAsrc_id", fasrc));
        cmd.Parameters.Add(new SqlParameter("@FAtgt_id", fatgt));
        cmd.Parameters.Add(new SqlParameter("@FSsrc_id", fssrc));
        cmd.Parameters.Add(new SqlParameter("@FStgt_id", fstgt));
        cmd.Parameters.Add(new SqlParameter("@Hsrc_id", hsrc));
        cmd.Parameters.Add(new SqlParameter("@Htgt_id", htgt));
        cmd.Parameters.Add(new SqlParameter("@Nsrc_id", nsrc));
        cmd.Parameters.Add(new SqlParameter("@Ntgt_id", ntgt));
        cmd.ExecuteNonQuery();
        conn2.Close();
        MessageBox.Show("All Done", "NGTest Automation", MessageBoxButtons.OK, MessageBoxIcon.Information);

    }
    else if (txtSystem.Text == "3")
    {
        string flocID = txtfloc.Text;
        string tlocID = txtTloc.Text;
        string fpracID = txtfpract.Text;
        string tpracID = txtTPract.Text;
        string centcode = txtCntrCode.Text;
        string imgcode = txtImgCode.Text;
        string lglname = txtLglName.Text;
        string addr1 = txtAddr1.Text;
        string addr2 = txtAddr2.Text;
        string zip = txtZip.Text;
        string npi = txtNPI.Text;
        string taxid = txtTaxID.Text;
        string phone = txtPhone.Text;
        string fax = txtFax.Text;
        string asrc = txtAdmin.Text;
        string atgt = txtTadmin.Text;
        string aasrc = txtAppAdmin.Text;
        string aatgt = txtTAppAdmin.Text;
        string bsrc = txtBilling.Text;
        string btgt = txtTBilling.Text;
        string bssrc = txtBillingSup.Text;
        string bstgt = txtTBillingSup.Text;
        string esrc = txtEHR.Text;
        string etgt = txtTEHR.Text;
        string fasrc = txtFDA.Text;
        string fatgt = txtTFDA.Text;
        string fssrc = txtFDSS.Text;
        string fstgt = txtTFDSS.Text;
        string hsrc = txtHD.Text;
        string htgt = txtTHD.Text;
        string nsrc = txtNA.Text;
        string ntgt = txtTNA.Text;

        SqlConnection conn3 = new SqlConnection("Server=DC-NEXGN-DB-01;Integrated Security=SSPI;Initial Catalog=NGProd");
        conn3.Open();
        SqlCommand cmd = new SqlCommand("NG_Copy", conn3);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add(new SqlParameter("@Flocation_id", flocID));
        cmd.Parameters.Add(new SqlParameter("@Tlocation_id", tlocID));
        cmd.Parameters.Add(new SqlParameter("@Fpractice_ID", fpracID));
        cmd.Parameters.Add(new SqlParameter("@Tpractice_id", tpracID));
        cmd.Parameters.Add(new SqlParameter("@center_code", centcode));
        cmd.Parameters.Add(new SqlParameter("@imagine_code", imgcode));
        cmd.Parameters.Add(new SqlParameter("@legal_name", lglname));
        cmd.Parameters.Add(new SqlParameter("@address1", addr1));
        cmd.Parameters.Add(new SqlParameter("@address2", addr2));
        cmd.Parameters.Add(new SqlParameter("@zipfull", zip));
        cmd.Parameters.Add(new SqlParameter("@NPI", npi));
        cmd.Parameters.Add(new SqlParameter("@TaxID", taxid));
        cmd.Parameters.Add(new SqlParameter("@Phone", phone));
        cmd.Parameters.Add(new SqlParameter("@Fax", fax));
        cmd.Parameters.Add(new SqlParameter("@Asrc_id", asrc));
        cmd.Parameters.Add(new SqlParameter("@Atgt_id", atgt));
        cmd.Parameters.Add(new SqlParameter("@AAsrc_id", aasrc));
        cmd.Parameters.Add(new SqlParameter("@AAtgt_id", aatgt));
        cmd.Parameters.Add(new SqlParameter("@Bsrc_id", bsrc));
        cmd.Parameters.Add(new SqlParameter("@Btgt_id", btgt));
        cmd.Parameters.Add(new SqlParameter("@BSsrc_id", bssrc));
        cmd.Parameters.Add(new SqlParameter("@BStgt_id", bstgt));
        cmd.Parameters.Add(new SqlParameter("@Esrc_id", esrc));
        cmd.Parameters.Add(new SqlParameter("@Etgt_id", etgt));
        cmd.Parameters.Add(new SqlParameter("@FAsrc_id", fasrc));
        cmd.Parameters.Add(new SqlParameter("@FAtgt_id", fatgt));
        cmd.Parameters.Add(new SqlParameter("@FSsrc_id", fssrc));
        cmd.Parameters.Add(new SqlParameter("@FStgt_id", fstgt));
        cmd.Parameters.Add(new SqlParameter("@Hsrc_id", hsrc));
        cmd.Parameters.Add(new SqlParameter("@Htgt_id", htgt));
        cmd.Parameters.Add(new SqlParameter("@Nsrc_id", nsrc));
        cmd.Parameters.Add(new SqlParameter("@Ntgt_id", ntgt));
        cmd.ExecuteNonQuery();
        conn3.Close();
        MessageBox.Show("All Done", "NGProd Automation", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }
    else if (txtSystem.Text == "4")
    {
        string flocID = txtfloc.Text;
        string tlocID = txtTloc.Text;
        string fpracID = txtfpract.Text;
        string tpracID = txtTPract.Text;
        string centcode = txtCntrCode.Text;
        string imgcode = txtImgCode.Text;
        string lglname = txtLglName.Text;
        string addr1 = txtAddr1.Text;
        string addr2 = txtAddr2.Text;
        string zip = txtZip.Text;
        string npi = txtNPI.Text;
        string taxid = txtTaxID.Text;
        string phone = txtPhone.Text;
        string fax = txtFax.Text;
        string asrc = txtAdmin.Text;
        string atgt = txtTadmin.Text;
        string aasrc = txtAppAdmin.Text;
        string aatgt = txtTAppAdmin.Text;
        string bsrc = txtBilling.Text;
        string btgt = txtTBilling.Text;
        string bssrc = txtBillingSup.Text;
        string bstgt = txtTBillingSup.Text;
        string esrc = txtEHR.Text;
        string etgt = txtTEHR.Text;
        string fasrc = txtFDA.Text;
        string fatgt = txtTFDA.Text;
        string fssrc = txtFDSS.Text;
        string fstgt = txtTFDSS.Text;
        string hsrc = txtHD.Text;
        string htgt = txtTHD.Text;
        string nsrc = txtNA.Text;
        string ntgt = txtTNA.Text;

        SqlConnection conn4 = new SqlConnection("Server=TS-NEXGN-DB-02;Integrated Security=SSPI;Initial Catalog=NGDemo");
        conn4.Open();
        SqlCommand cmd = new SqlCommand("NG_Copy", conn4);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add(new SqlParameter("@Flocation_id", flocID));
        cmd.Parameters.Add(new SqlParameter("@Tlocation_id", tlocID));
        cmd.Parameters.Add(new SqlParameter("@Fpractice_ID", fpracID));
        cmd.Parameters.Add(new SqlParameter("@Tpractice_id", tpracID));
        cmd.Parameters.Add(new SqlParameter("@center_code", centcode));
        cmd.Parameters.Add(new SqlParameter("@imagine_code", imgcode));
        cmd.Parameters.Add(new SqlParameter("@legal_name", lglname));
        cmd.Parameters.Add(new SqlParameter("@address1", addr1));
        cmd.Parameters.Add(new SqlParameter("@address2", addr2));
        cmd.Parameters.Add(new SqlParameter("@zipfull", zip));
        cmd.Parameters.Add(new SqlParameter("@NPI", npi));
        cmd.Parameters.Add(new SqlParameter("@TaxID", taxid));
        cmd.Parameters.Add(new SqlParameter("@Phone", phone));
        cmd.Parameters.Add(new SqlParameter("@Fax", fax));
        cmd.Parameters.Add(new SqlParameter("@Asrc_id", asrc));
        cmd.Parameters.Add(new SqlParameter("@Atgt_id", atgt));
        cmd.Parameters.Add(new SqlParameter("@AAsrc_id", aasrc));
        cmd.Parameters.Add(new SqlParameter("@AAtgt_id", aatgt));
        cmd.Parameters.Add(new SqlParameter("@Bsrc_id", bsrc));
        cmd.Parameters.Add(new SqlParameter("@Btgt_id", btgt));
        cmd.Parameters.Add(new SqlParameter("@BSsrc_id", bssrc));
        cmd.Parameters.Add(new SqlParameter("@BStgt_id", bstgt));
        cmd.Parameters.Add(new SqlParameter("@Esrc_id", esrc));
        cmd.Parameters.Add(new SqlParameter("@Etgt_id", etgt));
        cmd.Parameters.Add(new SqlParameter("@FAsrc_id", fasrc));
        cmd.Parameters.Add(new SqlParameter("@FAtgt_id", fatgt));
        cmd.Parameters.Add(new SqlParameter("@FSsrc_id", fssrc));
        cmd.Parameters.Add(new SqlParameter("@FStgt_id", fstgt));
        cmd.Parameters.Add(new SqlParameter("@Hsrc_id", hsrc));
        cmd.Parameters.Add(new SqlParameter("@Htgt_id", htgt));
        cmd.Parameters.Add(new SqlParameter("@Nsrc_id", nsrc));
        cmd.Parameters.Add(new SqlParameter("@Ntgt_id", ntgt));
        cmd.ExecuteNonQuery();
        conn4.Close();
        MessageBox.Show("All Done", "NGDemo Automation", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }
}*/



}
