﻿namespace AzuraAppDevUtility
{
    partial class frmNewPractice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCntrCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtImgCode = new System.Windows.Forms.TextBox();
            this.lblLglName = new System.Windows.Forms.Label();
            this.txtLglName = new System.Windows.Forms.TextBox();
            this.lblAddr1 = new System.Windows.Forms.Label();
            this.txtAddr1 = new System.Windows.Forms.TextBox();
            this.lblAddr2 = new System.Windows.Forms.Label();
            this.txtAddr2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtState = new System.Windows.Forms.TextBox();
            this.lblZip = new System.Windows.Forms.Label();
            this.txtZip = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNPI = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTaxID = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTPract = new System.Windows.Forms.TextBox();
            this.txtfpract = new System.Windows.Forms.TextBox();
            this.txtfloc = new System.Windows.Forms.TextBox();
            this.txtTloc = new System.Windows.Forms.TextBox();
            this.btnRetrieve = new System.Windows.Forms.Button();
            this.btnModify = new System.Windows.Forms.Button();
            this.btnAutomate = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtFax = new System.Windows.Forms.TextBox();
            this.txtAdmin = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtAppAdmin = new System.Windows.Forms.TextBox();
            this.txtBilling = new System.Windows.Forms.TextBox();
            this.txtBillingSup = new System.Windows.Forms.TextBox();
            this.txtEHR = new System.Windows.Forms.TextBox();
            this.txtFDA = new System.Windows.Forms.TextBox();
            this.txtFDSS = new System.Windows.Forms.TextBox();
            this.txtTFDSS = new System.Windows.Forms.TextBox();
            this.txtTFDA = new System.Windows.Forms.TextBox();
            this.txtTEHR = new System.Windows.Forms.TextBox();
            this.txtTBillingSup = new System.Windows.Forms.TextBox();
            this.txtTBilling = new System.Windows.Forms.TextBox();
            this.txtTAppAdmin = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTadmin = new System.Windows.Forms.TextBox();
            this.btnDev = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnProd = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtTNA = new System.Windows.Forms.TextBox();
            this.txtTHD = new System.Windows.Forms.TextBox();
            this.txtNA = new System.Windows.Forms.TextBox();
            this.txtHD = new System.Windows.Forms.TextBox();
            this.txtSystem = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnDemo = new System.Windows.Forms.Button();
            this.chkASC = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtCntrCode
            // 
            this.txtCntrCode.Location = new System.Drawing.Point(85, 5);
            this.txtCntrCode.Name = "txtCntrCode";
            this.txtCntrCode.Size = new System.Drawing.Size(56, 20);
            this.txtCntrCode.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Center Code";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Imagine Code";
            // 
            // txtImgCode
            // 
            this.txtImgCode.Location = new System.Drawing.Point(79, 19);
            this.txtImgCode.Name = "txtImgCode";
            this.txtImgCode.Size = new System.Drawing.Size(219, 20);
            this.txtImgCode.TabIndex = 2;
            // 
            // lblLglName
            // 
            this.lblLglName.AutoSize = true;
            this.lblLglName.Location = new System.Drawing.Point(7, 52);
            this.lblLglName.Name = "lblLglName";
            this.lblLglName.Size = new System.Drawing.Size(64, 13);
            this.lblLglName.TabIndex = 5;
            this.lblLglName.Text = "Legal Name";
            // 
            // txtLglName
            // 
            this.txtLglName.Location = new System.Drawing.Point(79, 45);
            this.txtLglName.Name = "txtLglName";
            this.txtLglName.ReadOnly = true;
            this.txtLglName.Size = new System.Drawing.Size(219, 20);
            this.txtLglName.TabIndex = 4;
            this.txtLglName.TextChanged += new System.EventHandler(this.txtLglName_TextChanged);
            // 
            // lblAddr1
            // 
            this.lblAddr1.AutoSize = true;
            this.lblAddr1.Location = new System.Drawing.Point(7, 78);
            this.lblAddr1.Name = "lblAddr1";
            this.lblAddr1.Size = new System.Drawing.Size(54, 13);
            this.lblAddr1.TabIndex = 7;
            this.lblAddr1.Text = "Address 1";
            // 
            // txtAddr1
            // 
            this.txtAddr1.Location = new System.Drawing.Point(79, 71);
            this.txtAddr1.Name = "txtAddr1";
            this.txtAddr1.ReadOnly = true;
            this.txtAddr1.Size = new System.Drawing.Size(219, 20);
            this.txtAddr1.TabIndex = 6;
            this.txtAddr1.TextChanged += new System.EventHandler(this.txtAddr1_TextChanged);
            // 
            // lblAddr2
            // 
            this.lblAddr2.AutoSize = true;
            this.lblAddr2.Location = new System.Drawing.Point(7, 104);
            this.lblAddr2.Name = "lblAddr2";
            this.lblAddr2.Size = new System.Drawing.Size(54, 13);
            this.lblAddr2.TabIndex = 9;
            this.lblAddr2.Text = "Address 2";
            // 
            // txtAddr2
            // 
            this.txtAddr2.Location = new System.Drawing.Point(79, 97);
            this.txtAddr2.Name = "txtAddr2";
            this.txtAddr2.ReadOnly = true;
            this.txtAddr2.Size = new System.Drawing.Size(219, 20);
            this.txtAddr2.TabIndex = 8;
            this.txtAddr2.TextChanged += new System.EventHandler(this.txtAddr2_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 130);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "City";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(79, 123);
            this.txtCity.Name = "txtCity";
            this.txtCity.ReadOnly = true;
            this.txtCity.Size = new System.Drawing.Size(219, 20);
            this.txtCity.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "State";
            // 
            // txtState
            // 
            this.txtState.Location = new System.Drawing.Point(79, 149);
            this.txtState.Name = "txtState";
            this.txtState.ReadOnly = true;
            this.txtState.Size = new System.Drawing.Size(219, 20);
            this.txtState.TabIndex = 12;
            // 
            // lblZip
            // 
            this.lblZip.AutoSize = true;
            this.lblZip.Location = new System.Drawing.Point(10, 182);
            this.lblZip.Name = "lblZip";
            this.lblZip.Size = new System.Drawing.Size(22, 13);
            this.lblZip.TabIndex = 15;
            this.lblZip.Text = "Zip";
            // 
            // txtZip
            // 
            this.txtZip.Location = new System.Drawing.Point(79, 175);
            this.txtZip.Name = "txtZip";
            this.txtZip.ReadOnly = true;
            this.txtZip.Size = new System.Drawing.Size(219, 20);
            this.txtZip.TabIndex = 14;
            this.txtZip.TextChanged += new System.EventHandler(this.txtZip_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 208);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "NPI";
            // 
            // txtNPI
            // 
            this.txtNPI.Location = new System.Drawing.Point(79, 201);
            this.txtNPI.Name = "txtNPI";
            this.txtNPI.ReadOnly = true;
            this.txtNPI.Size = new System.Drawing.Size(219, 20);
            this.txtNPI.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 234);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Tax ID";
            // 
            // txtTaxID
            // 
            this.txtTaxID.Location = new System.Drawing.Point(79, 227);
            this.txtTaxID.Name = "txtTaxID";
            this.txtTaxID.ReadOnly = true;
            this.txtTaxID.Size = new System.Drawing.Size(219, 20);
            this.txtTaxID.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 40);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "From Practice";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 140);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "To Practice";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 66);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "From Location";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 166);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "To Location";
            // 
            // txtTPract
            // 
            this.txtTPract.Location = new System.Drawing.Point(82, 133);
            this.txtTPract.Name = "txtTPract";
            this.txtTPract.ReadOnly = true;
            this.txtTPract.Size = new System.Drawing.Size(77, 20);
            this.txtTPract.TabIndex = 24;
            // 
            // txtfpract
            // 
            this.txtfpract.Location = new System.Drawing.Point(82, 33);
            this.txtfpract.Name = "txtfpract";
            this.txtfpract.ReadOnly = true;
            this.txtfpract.Size = new System.Drawing.Size(77, 20);
            this.txtfpract.TabIndex = 25;
            // 
            // txtfloc
            // 
            this.txtfloc.Location = new System.Drawing.Point(82, 59);
            this.txtfloc.Name = "txtfloc";
            this.txtfloc.ReadOnly = true;
            this.txtfloc.Size = new System.Drawing.Size(305, 20);
            this.txtfloc.TabIndex = 26;
            // 
            // txtTloc
            // 
            this.txtTloc.Location = new System.Drawing.Point(82, 159);
            this.txtTloc.Name = "txtTloc";
            this.txtTloc.ReadOnly = true;
            this.txtTloc.Size = new System.Drawing.Size(305, 20);
            this.txtTloc.TabIndex = 27;
            // 
            // btnRetrieve
            // 
            this.btnRetrieve.Location = new System.Drawing.Point(147, 5);
            this.btnRetrieve.Name = "btnRetrieve";
            this.btnRetrieve.Size = new System.Drawing.Size(95, 20);
            this.btnRetrieve.TabIndex = 28;
            this.btnRetrieve.Text = "Retrieve Data";
            this.btnRetrieve.UseVisualStyleBackColor = true;
            this.btnRetrieve.Click += new System.EventHandler(this.btnRetrieve_Click);
            // 
            // btnModify
            // 
            this.btnModify.Location = new System.Drawing.Point(191, 58);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(126, 21);
            this.btnModify.TabIndex = 29;
            this.btnModify.Text = "Modify Data";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Visible = false;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // btnAutomate
            // 
            this.btnAutomate.Location = new System.Drawing.Point(785, 368);
            this.btnAutomate.Name = "btnAutomate";
            this.btnAutomate.Size = new System.Drawing.Size(83, 23);
            this.btnAutomate.TabIndex = 30;
            this.btnAutomate.Text = "Automate NG";
            this.btnAutomate.UseVisualStyleBackColor = true;
            this.btnAutomate.Visible = false;
            this.btnAutomate.Click += new System.EventHandler(this.btnAutomate_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 256);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(38, 13);
            this.label15.TabIndex = 31;
            this.label15.Text = "Phone";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 283);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(24, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Fax";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(79, 253);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.ReadOnly = true;
            this.txtPhone.Size = new System.Drawing.Size(219, 20);
            this.txtPhone.TabIndex = 33;
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(79, 279);
            this.txtFax.Name = "txtFax";
            this.txtFax.ReadOnly = true;
            this.txtFax.Size = new System.Drawing.Size(219, 20);
            this.txtFax.TabIndex = 34;
            // 
            // txtAdmin
            // 
            this.txtAdmin.Location = new System.Drawing.Point(82, 83);
            this.txtAdmin.Name = "txtAdmin";
            this.txtAdmin.ReadOnly = true;
            this.txtAdmin.Size = new System.Drawing.Size(44, 20);
            this.txtAdmin.TabIndex = 35;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 90);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 13);
            this.label17.TabIndex = 36;
            this.label17.Text = "Security IDs";
            // 
            // txtAppAdmin
            // 
            this.txtAppAdmin.Location = new System.Drawing.Point(132, 83);
            this.txtAppAdmin.Name = "txtAppAdmin";
            this.txtAppAdmin.ReadOnly = true;
            this.txtAppAdmin.Size = new System.Drawing.Size(44, 20);
            this.txtAppAdmin.TabIndex = 37;
            // 
            // txtBilling
            // 
            this.txtBilling.Location = new System.Drawing.Point(182, 83);
            this.txtBilling.Name = "txtBilling";
            this.txtBilling.ReadOnly = true;
            this.txtBilling.Size = new System.Drawing.Size(44, 20);
            this.txtBilling.TabIndex = 38;
            // 
            // txtBillingSup
            // 
            this.txtBillingSup.Location = new System.Drawing.Point(232, 83);
            this.txtBillingSup.Name = "txtBillingSup";
            this.txtBillingSup.ReadOnly = true;
            this.txtBillingSup.Size = new System.Drawing.Size(44, 20);
            this.txtBillingSup.TabIndex = 39;
            // 
            // txtEHR
            // 
            this.txtEHR.Location = new System.Drawing.Point(282, 83);
            this.txtEHR.Name = "txtEHR";
            this.txtEHR.ReadOnly = true;
            this.txtEHR.Size = new System.Drawing.Size(44, 20);
            this.txtEHR.TabIndex = 40;
            // 
            // txtFDA
            // 
            this.txtFDA.Location = new System.Drawing.Point(332, 83);
            this.txtFDA.Name = "txtFDA";
            this.txtFDA.ReadOnly = true;
            this.txtFDA.Size = new System.Drawing.Size(44, 20);
            this.txtFDA.TabIndex = 41;
            // 
            // txtFDSS
            // 
            this.txtFDSS.Location = new System.Drawing.Point(382, 83);
            this.txtFDSS.Name = "txtFDSS";
            this.txtFDSS.ReadOnly = true;
            this.txtFDSS.Size = new System.Drawing.Size(44, 20);
            this.txtFDSS.TabIndex = 42;
            // 
            // txtTFDSS
            // 
            this.txtTFDSS.Location = new System.Drawing.Point(382, 185);
            this.txtTFDSS.Name = "txtTFDSS";
            this.txtTFDSS.ReadOnly = true;
            this.txtTFDSS.Size = new System.Drawing.Size(44, 20);
            this.txtTFDSS.TabIndex = 50;
            // 
            // txtTFDA
            // 
            this.txtTFDA.Location = new System.Drawing.Point(332, 185);
            this.txtTFDA.Name = "txtTFDA";
            this.txtTFDA.ReadOnly = true;
            this.txtTFDA.Size = new System.Drawing.Size(44, 20);
            this.txtTFDA.TabIndex = 49;
            // 
            // txtTEHR
            // 
            this.txtTEHR.Location = new System.Drawing.Point(282, 185);
            this.txtTEHR.Name = "txtTEHR";
            this.txtTEHR.ReadOnly = true;
            this.txtTEHR.Size = new System.Drawing.Size(44, 20);
            this.txtTEHR.TabIndex = 48;
            // 
            // txtTBillingSup
            // 
            this.txtTBillingSup.Location = new System.Drawing.Point(232, 185);
            this.txtTBillingSup.Name = "txtTBillingSup";
            this.txtTBillingSup.ReadOnly = true;
            this.txtTBillingSup.Size = new System.Drawing.Size(44, 20);
            this.txtTBillingSup.TabIndex = 47;
            // 
            // txtTBilling
            // 
            this.txtTBilling.Location = new System.Drawing.Point(182, 185);
            this.txtTBilling.Name = "txtTBilling";
            this.txtTBilling.ReadOnly = true;
            this.txtTBilling.Size = new System.Drawing.Size(44, 20);
            this.txtTBilling.TabIndex = 46;
            // 
            // txtTAppAdmin
            // 
            this.txtTAppAdmin.Location = new System.Drawing.Point(132, 185);
            this.txtTAppAdmin.Name = "txtTAppAdmin";
            this.txtTAppAdmin.ReadOnly = true;
            this.txtTAppAdmin.Size = new System.Drawing.Size(44, 20);
            this.txtTAppAdmin.TabIndex = 45;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 192);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(64, 13);
            this.label18.TabIndex = 44;
            this.label18.Text = "Security IDs";
            // 
            // txtTadmin
            // 
            this.txtTadmin.Location = new System.Drawing.Point(82, 185);
            this.txtTadmin.Name = "txtTadmin";
            this.txtTadmin.ReadOnly = true;
            this.txtTadmin.Size = new System.Drawing.Size(44, 20);
            this.txtTadmin.TabIndex = 43;
            // 
            // btnDev
            // 
            this.btnDev.Location = new System.Drawing.Point(429, 118);
            this.btnDev.Name = "btnDev";
            this.btnDev.Size = new System.Drawing.Size(83, 23);
            this.btnDev.TabIndex = 51;
            this.btnDev.Text = "Development";
            this.btnDev.UseVisualStyleBackColor = true;
            this.btnDev.Visible = false;
            this.btnDev.Click += new System.EventHandler(this.btnDev_Click);
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(518, 118);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(85, 23);
            this.btnTest.TabIndex = 52;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Visible = false;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnProd
            // 
            this.btnProd.Location = new System.Drawing.Point(609, 118);
            this.btnProd.Name = "btnProd";
            this.btnProd.Size = new System.Drawing.Size(96, 23);
            this.btnProd.TabIndex = 53;
            this.btnProd.Text = "Production";
            this.btnProd.UseVisualStyleBackColor = true;
            this.btnProd.Visible = false;
            this.btnProd.Click += new System.EventHandler(this.btnProd_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtTNA);
            this.groupBox1.Controls.Add(this.txtTHD);
            this.groupBox1.Controls.Add(this.txtNA);
            this.groupBox1.Controls.Add(this.txtHD);
            this.groupBox1.Controls.Add(this.txtSystem);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtTFDSS);
            this.groupBox1.Controls.Add(this.txtTPract);
            this.groupBox1.Controls.Add(this.txtTFDA);
            this.groupBox1.Controls.Add(this.txtfpract);
            this.groupBox1.Controls.Add(this.txtTEHR);
            this.groupBox1.Controls.Add(this.txtfloc);
            this.groupBox1.Controls.Add(this.txtTBillingSup);
            this.groupBox1.Controls.Add(this.txtTloc);
            this.groupBox1.Controls.Add(this.txtTBilling);
            this.groupBox1.Controls.Add(this.txtAdmin);
            this.groupBox1.Controls.Add(this.txtTAppAdmin);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.txtAppAdmin);
            this.groupBox1.Controls.Add(this.txtTadmin);
            this.groupBox1.Controls.Add(this.txtBilling);
            this.groupBox1.Controls.Add(this.txtFDSS);
            this.groupBox1.Controls.Add(this.txtBillingSup);
            this.groupBox1.Controls.Add(this.txtFDA);
            this.groupBox1.Controls.Add(this.txtEHR);
            this.groupBox1.Location = new System.Drawing.Point(329, 147);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(539, 214);
            this.groupBox1.TabIndex = 54;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "NextGen Data";
            this.groupBox1.Visible = false;
            // 
            // txtTNA
            // 
            this.txtTNA.Location = new System.Drawing.Point(482, 185);
            this.txtTNA.Name = "txtTNA";
            this.txtTNA.ReadOnly = true;
            this.txtTNA.Size = new System.Drawing.Size(44, 20);
            this.txtTNA.TabIndex = 55;
            // 
            // txtTHD
            // 
            this.txtTHD.Location = new System.Drawing.Point(432, 185);
            this.txtTHD.Name = "txtTHD";
            this.txtTHD.ReadOnly = true;
            this.txtTHD.Size = new System.Drawing.Size(44, 20);
            this.txtTHD.TabIndex = 54;
            // 
            // txtNA
            // 
            this.txtNA.Location = new System.Drawing.Point(482, 83);
            this.txtNA.Name = "txtNA";
            this.txtNA.ReadOnly = true;
            this.txtNA.Size = new System.Drawing.Size(44, 20);
            this.txtNA.TabIndex = 53;
            // 
            // txtHD
            // 
            this.txtHD.Location = new System.Drawing.Point(432, 83);
            this.txtHD.Name = "txtHD";
            this.txtHD.ReadOnly = true;
            this.txtHD.Size = new System.Drawing.Size(44, 20);
            this.txtHD.TabIndex = 52;
            // 
            // txtSystem
            // 
            this.txtSystem.Location = new System.Drawing.Point(482, 13);
            this.txtSystem.Name = "txtSystem";
            this.txtSystem.ReadOnly = true;
            this.txtSystem.Size = new System.Drawing.Size(44, 20);
            this.txtSystem.TabIndex = 51;
            this.txtSystem.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtFax);
            this.groupBox2.Controls.Add(this.txtPhone);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtTaxID);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtNPI);
            this.groupBox2.Controls.Add(this.txtImgCode);
            this.groupBox2.Controls.Add(this.lblZip);
            this.groupBox2.Controls.Add(this.txtZip);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtState);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtCity);
            this.groupBox2.Controls.Add(this.lblAddr2);
            this.groupBox2.Controls.Add(this.txtAddr2);
            this.groupBox2.Controls.Add(this.lblAddr1);
            this.groupBox2.Controls.Add(this.txtAddr1);
            this.groupBox2.Controls.Add(this.lblLglName);
            this.groupBox2.Controls.Add(this.txtLglName);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(6, 85);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(311, 305);
            this.groupBox2.TabIndex = 55;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Imagine Data";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(248, 5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(69, 20);
            this.btnClear.TabIndex = 56;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnDemo
            // 
            this.btnDemo.Location = new System.Drawing.Point(338, 118);
            this.btnDemo.Name = "btnDemo";
            this.btnDemo.Size = new System.Drawing.Size(83, 23);
            this.btnDemo.TabIndex = 57;
            this.btnDemo.Text = "Demo";
            this.btnDemo.UseVisualStyleBackColor = true;
            this.btnDemo.Visible = false;
            this.btnDemo.Click += new System.EventHandler(this.btn_Demo_Click);
            // 
            // chkASC
            // 
            this.chkASC.AutoSize = true;
            this.chkASC.Location = new System.Drawing.Point(329, 7);
            this.chkASC.Name = "chkASC";
            this.chkASC.Size = new System.Drawing.Size(89, 17);
            this.chkASC.TabIndex = 58;
            this.chkASC.Text = "ASC Practice";
            this.chkASC.UseVisualStyleBackColor = true;
            // 
            // frmNewPractice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 527);
            this.Controls.Add(this.chkASC);
            this.Controls.Add(this.btnDemo);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnProd);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.btnDev);
            this.Controls.Add(this.btnAutomate);
            this.Controls.Add(this.btnModify);
            this.Controls.Add(this.btnRetrieve);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCntrCode);
            this.Name = "frmNewPractice";
            this.Text = "Location & Practice Copy";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCntrCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtImgCode;
        private System.Windows.Forms.Label lblLglName;
        private System.Windows.Forms.TextBox txtLglName;
        private System.Windows.Forms.Label lblAddr1;
        private System.Windows.Forms.TextBox txtAddr1;
        private System.Windows.Forms.Label lblAddr2;
        private System.Windows.Forms.TextBox txtAddr2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtState;
        private System.Windows.Forms.Label lblZip;
        private System.Windows.Forms.TextBox txtZip;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtNPI;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtTaxID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtTPract;
        private System.Windows.Forms.TextBox txtfpract;
        private System.Windows.Forms.TextBox txtfloc;
        private System.Windows.Forms.TextBox txtTloc;
        private System.Windows.Forms.Button btnRetrieve;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnAutomate;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtFax;
        private System.Windows.Forms.TextBox txtAdmin;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtAppAdmin;
        private System.Windows.Forms.TextBox txtBilling;
        private System.Windows.Forms.TextBox txtBillingSup;
        private System.Windows.Forms.TextBox txtEHR;
        private System.Windows.Forms.TextBox txtFDA;
        private System.Windows.Forms.TextBox txtFDSS;
        private System.Windows.Forms.TextBox txtTFDSS;
        private System.Windows.Forms.TextBox txtTFDA;
        private System.Windows.Forms.TextBox txtTEHR;
        private System.Windows.Forms.TextBox txtTBillingSup;
        private System.Windows.Forms.TextBox txtTBilling;
        private System.Windows.Forms.TextBox txtTAppAdmin;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtTadmin;
        private System.Windows.Forms.Button btnDev;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Button btnProd;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtSystem;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.TextBox txtTNA;
        private System.Windows.Forms.TextBox txtTHD;
        private System.Windows.Forms.TextBox txtNA;
        private System.Windows.Forms.TextBox txtHD;
        private System.Windows.Forms.Button btnDemo;
        private System.Windows.Forms.CheckBox chkASC;
    }
}

