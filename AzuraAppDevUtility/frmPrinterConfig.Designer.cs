﻿
namespace AzuraAppDevUtility
{
    partial class frmPrinterConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnIPInfo = new System.Windows.Forms.Button();
            this.IP_Oct4 = new System.Windows.Forms.NumericUpDown();
            this.IP_Oct3 = new System.Windows.Forms.NumericUpDown();
            this.IP_Oct2 = new System.Windows.Forms.NumericUpDown();
            this.IP_Oct1 = new System.Windows.Forms.NumericUpDown();
            this.IP_Port = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbPractice = new System.Windows.Forms.ComboBox();
            this.cmbLocation = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.IP_Oct4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IP_Oct3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IP_Oct2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IP_Oct1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IP_Port)).BeginInit();
            this.SuspendLayout();
            // 
            // btnIPInfo
            // 
            this.btnIPInfo.Location = new System.Drawing.Point(845, 86);
            this.btnIPInfo.Name = "btnIPInfo";
            this.btnIPInfo.Size = new System.Drawing.Size(75, 23);
            this.btnIPInfo.TabIndex = 0;
            this.btnIPInfo.Text = "Edit";
            this.btnIPInfo.UseVisualStyleBackColor = true;
            this.btnIPInfo.Click += new System.EventHandler(this.btnIPInfo_Click);
            // 
            // IP_Oct4
            // 
            this.IP_Oct4.Location = new System.Drawing.Point(864, 46);
            this.IP_Oct4.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.IP_Oct4.Name = "IP_Oct4";
            this.IP_Oct4.ReadOnly = true;
            this.IP_Oct4.Size = new System.Drawing.Size(56, 20);
            this.IP_Oct4.TabIndex = 1;
            // 
            // IP_Oct3
            // 
            this.IP_Oct3.Location = new System.Drawing.Point(802, 46);
            this.IP_Oct3.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.IP_Oct3.Name = "IP_Oct3";
            this.IP_Oct3.ReadOnly = true;
            this.IP_Oct3.Size = new System.Drawing.Size(56, 20);
            this.IP_Oct3.TabIndex = 2;
            // 
            // IP_Oct2
            // 
            this.IP_Oct2.Location = new System.Drawing.Point(740, 46);
            this.IP_Oct2.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.IP_Oct2.Name = "IP_Oct2";
            this.IP_Oct2.ReadOnly = true;
            this.IP_Oct2.Size = new System.Drawing.Size(56, 20);
            this.IP_Oct2.TabIndex = 3;
            // 
            // IP_Oct1
            // 
            this.IP_Oct1.Location = new System.Drawing.Point(678, 46);
            this.IP_Oct1.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.IP_Oct1.Name = "IP_Oct1";
            this.IP_Oct1.ReadOnly = true;
            this.IP_Oct1.Size = new System.Drawing.Size(56, 20);
            this.IP_Oct1.TabIndex = 4;
            // 
            // IP_Port
            // 
            this.IP_Port.Location = new System.Drawing.Point(678, 88);
            this.IP_Port.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.IP_Port.Name = "IP_Port";
            this.IP_Port.ReadOnly = true;
            this.IP_Port.Size = new System.Drawing.Size(72, 20);
            this.IP_Port.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(600, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "IP Address";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(600, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Port";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(591, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Printer Information";
            // 
            // cmbPractice
            // 
            this.cmbPractice.FormattingEnabled = true;
            this.cmbPractice.Location = new System.Drawing.Point(118, 61);
            this.cmbPractice.Name = "cmbPractice";
            this.cmbPractice.Size = new System.Drawing.Size(387, 21);
            this.cmbPractice.TabIndex = 9;
            this.cmbPractice.SelectedIndexChanged += new System.EventHandler(this.cmbPractice_SelectedIndexChanged);
            // 
            // cmbLocation
            // 
            this.cmbLocation.FormattingEnabled = true;
            this.cmbLocation.Location = new System.Drawing.Point(118, 88);
            this.cmbLocation.Name = "cmbLocation";
            this.cmbLocation.Size = new System.Drawing.Size(387, 21);
            this.cmbLocation.TabIndex = 10;
            this.cmbLocation.SelectedIndexChanged += new System.EventHandler(this.cmbLocation_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(27, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Practice";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(27, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Location";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(27, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(463, 29);
            this.label6.TabIndex = 13;
            this.label6.Text = "Nextgen Wristband Printer Config Tool";
            // 
            // frmPrinterConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 134);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbLocation);
            this.Controls.Add(this.cmbPractice);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.IP_Port);
            this.Controls.Add(this.IP_Oct1);
            this.Controls.Add(this.IP_Oct2);
            this.Controls.Add(this.IP_Oct3);
            this.Controls.Add(this.IP_Oct4);
            this.Controls.Add(this.btnIPInfo);
            this.Name = "frmPrinterConfig";
            this.Text = "NG Printer Config";
            this.Load += new System.EventHandler(this.frmPrinterConfig_Load);
            ((System.ComponentModel.ISupportInitialize)(this.IP_Oct4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IP_Oct3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IP_Oct2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IP_Oct1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IP_Port)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnIPInfo;
        private System.Windows.Forms.NumericUpDown IP_Oct4;
        private System.Windows.Forms.NumericUpDown IP_Oct3;
        private System.Windows.Forms.NumericUpDown IP_Oct2;
        private System.Windows.Forms.NumericUpDown IP_Oct1;
        private System.Windows.Forms.NumericUpDown IP_Port;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbPractice;
        private System.Windows.Forms.ComboBox cmbLocation;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}