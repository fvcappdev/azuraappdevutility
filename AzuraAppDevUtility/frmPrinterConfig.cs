﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//New
using AzuraAppDevUtility;
using System.Configuration;

namespace AzuraAppDevUtility
{
    public partial class frmPrinterConfig : Form
    {
        public frmPrinterConfig()
        {
            InitializeComponent();
        }

        private bool practice_exist(string pract_id, string loc_id)
        {
            //SqlConnection connection = new SqlConnection("Data Source=AACEMRVRSQL; Integrated Security=SSPI; Initial Catalog=NGProd");
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
            {
                string query = string.Format("SELECT COUNT(*) AS [Cnt] FROM FVC_wristband_printer_config WHERE practice_id = '{0}' and location_id = '{1}'", pract_id, loc_id);
                int x = 0;
                bool exists;
                SqlCommand command = new SqlCommand
                {
                    CommandText = query,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    x = (int)reader["Cnt"];
                }

                if (x > 0)
                {
                    exists = true;
                }
                else
                {
                    exists = false;
                }
                connection.Close();
                return exists;
            }
        }

        private void btnIPInfo_Click(object sender, EventArgs e)
        {
            int num = 0;
            if (this.btnIPInfo.Text == "Edit")
            {
                num = 1;
                this.IP_Oct1.ReadOnly = false;
                this.IP_Oct2.ReadOnly = false;
                this.IP_Oct3.ReadOnly = false;
                this.IP_Oct4.ReadOnly = false;
                this.IP_Port.ReadOnly = false;
            }
            else if (this.btnIPInfo.Text == "Save")
            {
                num = 2;
                this.IP_Oct1.ReadOnly = true;
                this.IP_Oct2.ReadOnly = true;
                this.IP_Oct3.ReadOnly = true;
                this.IP_Oct4.ReadOnly = true;
                this.IP_Port.ReadOnly = true;
            }
            else
            {
                MessageBox.Show("I have no clue how you got here......", "Button Errored", MessageBoxButtons.OK);
            }
            switch (num)
            {
                case 1:
                    this.btnIPInfo.Text = "Save";
                    return;

                case 2:
                    {

                        string str = this.IP_Oct1.Value.ToString() + "." + this.IP_Oct2.Value.ToString() + "." + this.IP_Oct3.Value.ToString() + "." + this.IP_Oct4.Value.ToString();
                        string str1 = this.cmbPractice.SelectedText.ToString();     //Likely used to be populated w/ previous version of the form..
                        string str2 = this.IP_Port.Value.ToString();
                        string str3 = this.cmbPractice.SelectedValue.ToString();
                        string str4 = this.cmbLocation.SelectedValue.ToString();
                        string str5;
                        string str6 = str + ":" + str2;
                        if (practice_exist(str3, str4))
                        {
                            str5 = string.Format("UPDATE FVC_wristband_printer_config SET ip_address = '{0}', ip_port = '{1}' WHERE practice_id = '{2}' AND location_id = '{3}'", new object[] { str, str2, str3, str4 });
                        }
                        else
                        {
                            //str5 = string.Format("INSERT INTO [NGprod].[dbo].[FVC_wristband_printer_config] ([enterprise_id],[practice_id],[location_id],[ip_address],[ip_port],[printer_name],[created_ts],[last_modified_ts]) VALUES " + "('00001', '{0}', '{1}', '{2}', '{3}', '{4}',GETDATE(),GETDATE())", new object[] { str3, str4, str, str2, str1 });
                            str5 = string.Format("INSERT INTO FVC_wristband_printer_config ([enterprise_id],[practice_id],[location_id],[ip_address],[ip_port],[printer_name],[created_ts],[last_modified_ts]) VALUES " + "('00001', '{0}', '{1}', '{2}', '{3}', '{4}',GETDATE(),GETDATE())", new object[] { str3, str4, str, str2, str1 });
                        }
                        //SqlConnection connection = new SqlConnection("Data Source=AACEMRVRSQL; Integrated Security=SSPI; Initial Catalog=NGProd");
                        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
                        {
                            SqlCommand command = new SqlCommand
                            {
                                CommandText = str5,
                                CommandType = CommandType.Text,
                                Connection = connection
                            };
                            connection.Open();
                            command.ExecuteNonQuery();
                            connection.Close();
                        }
                        MessageBox.Show("IP Address and Port Information Saved To Database", "Update Complete", MessageBoxButtons.OK);
                        this.btnIPInfo.Text = "Edit";
                        //Commons.LogUser(this.Name, "Add Printer IP to FVC_wristband_printer_config", str6, str3, str4);
                        Commons.LogUser2(this.Name, "Add Printer IP to FVC_wristband_printer_config", "", "", "", str6, str3, str4);
                        return;
                    }
            }
            MessageBox.Show("I have no clue how you got here......", "Update Errored", MessageBoxButtons.OK);
        }

        private void frmPrinterConfig_Load(object sender, EventArgs e)
        {
            string str = "SELECT DISTINCT practice_id, practice_name FROM practice WHERE delete_ind <> 'Y' AND practice_id NOT IN ('0001')";
            //SqlConnection connection = new SqlConnection("Data Source=AACEMRVRSQL; Integrated Security=SSPI; Initial Catalog=NGProd");
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    CommandText = str,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                this.cmbPractice.DataSource = null;
                this.cmbPractice.Items.Clear();
                Dictionary<string, string> dataSourceP = new Dictionary<string, string>();
                while (reader.Read())
                {
                    string value = (string)reader["practice_id"];
                    string key = (string)reader["practice_name"];
                    dataSourceP.Add(key, value);
                }
                connection.Close();
                this.cmbPractice.DisplayMember = "Key";
                this.cmbPractice.ValueMember = "Value";
                this.cmbPractice.DataSource = new BindingSource(dataSourceP, null);
            }
            //Commons.real_user_name = Environment.UserName.ToString();     //Done in frmMain.cs now as good global spot.
            //MessageBox.Show("real_user=" + real_user_name);
            // if (!Commons.CheckUser())
            // {
            //   MessageBox.Show("You do not have permissions to use this tool.");
            // this.Close();
            // }

            //// get service account username password for NG
            //string userName = ConfigurationManager.AppSettings.Get("service_account_userName");
            //string password = ConfigurationManager.AppSettings.Get("service_account_password");

            //// Impersonate user
            //ImpersonationDemo.Impersonation(userName, password);
        }

        private void cmbPractice_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cmbPractice.SelectedValue != null)
            {
                string str = string.Format("SELECT l.location_id, l.location_name FROM location_mstr l INNER JOIN practice_location_xref pl ON l.location_id=pl.location_id WHERE pl.practice_id = {0}", this.cmbPractice.SelectedValue);
                //SqlConnection connection = new SqlConnection("Data Source=AACEMRVRSQL; Integrated Security=SSPI; Initial Catalog=NGProd");
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
                {
                    SqlCommand command = new SqlCommand
                    {
                        CommandText = str,
                        CommandType = CommandType.Text,
                        Connection = connection
                    };
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    this.cmbLocation.DataSource = null;
                    this.cmbLocation.Items.Clear();
                    Dictionary<string, string> dataSource = new Dictionary<string, string>();
                    while (reader.Read())
                    {
                        Guid guid = (Guid)reader["location_id"];
                        string key = (string)reader["location_name"];
                        dataSource.Add(key, guid.ToString());
                    }
                    connection.Close();
                    this.cmbLocation.DisplayMember = "Key";
                    this.cmbLocation.ValueMember = "Value";
                    this.cmbLocation.DataSource = new BindingSource(dataSource, null);
                }
            }
        }

        private void cmbLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            string str = this.cmbPractice.SelectedValue.ToString();
            string str2 = "";
            string str3 = "0.0.0.0";
            string s = "9100";
            if (this.cmbLocation.SelectedValue != null)
            {
                str2 = this.cmbLocation.SelectedValue.ToString();
                string str5 = string.Format("SELECT ip_address, ip_port FROM FVC_wristband_printer_config WHERE practice_id = '{0}' AND location_id = '{1}'", str, str2);
                //SqlConnection connection = new SqlConnection("Data Source=AACEMRVRSQL; Integrated Security=SSPI; Initial Catalog=NGProd");
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NGConnString"].ConnectionString))
                {
                    SqlCommand command = new SqlCommand
                    {
                        CommandText = str5,
                        CommandType = CommandType.Text,
                        Connection = connection
                    };
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        str3 = (string)reader["ip_address"];
                        s = (string)reader["ip_port"];
                    }
                    connection.Close();
                }
            }
            string[] strArray = str3.Split(new char[] { '.' });
            this.IP_Oct1.Value = int.Parse(strArray[0]);
            this.IP_Oct2.Value = int.Parse(strArray[1]);
            this.IP_Oct3.Value = int.Parse(strArray[2]);
            this.IP_Oct4.Value = int.Parse(strArray[3]);
            this.IP_Port.Value = int.Parse(s);
        }
    }
}
