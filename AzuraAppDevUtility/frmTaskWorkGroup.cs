﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Threading;

// 1. need to do DEVL first,


namespace AzuraAppDevUtility
{
    public partial class frmTaskWorkgroup : Form
    {
        public frmTaskWorkgroup()
        {
            InitializeComponent();

            btnBackupTable.Enabled = true;
            btnTruncate.Enabled = false;
            btnCopy.Enabled = false;

            radioButton2.Checked = false;
            radioButton1.Checked = true; //default DEVL checked
        }

        
            
        String DataSystem = "NGDEVLConnString";  // control DEVL and TEST
       
        // control which system is copied
        int test_copy = 0;
        int devl_copy = 0;

        private void RefreshDataGrid()
        {

            string queryString = @"SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS num,

                                [enterprise_id]
                            ,[practice_id]
                             ,[workgroup_id]
                            ,[workgroup_name]
                            ,[workgroup_desc]
                           ,[note]
                           ,[delete_ind]
                           ,[create_timestamp]
                           ,[created_by]
                           ,[modify_timestamp]
                           ,[modified_by]
                           ,[former_group_id]
                           ,[workgroup_type]
		                   FROM dbo.[task_workgroup_mstr]

                            SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS num,
                            [practice_id]
                           ,[task_id]
                           ,[workgroup_id]
                           ,[create_timestamp]
                           ,[created_by]
                           ,[modify_timestamp]
                           ,[modified_by]
                           ,[response_type]
                           ,[enterprise_id]
		                    FROM dbo.[task_workgroup_assigned_to]

                            SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS num, 
                            [workgroup_id]
                           ,[user_id]
                           ,[created_by]
                           ,[create_timestamp]
		                   FROM dbo.[task_workgroup_user_xref]

                        ";


            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[DataSystem].ConnectionString))
            {

                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();

                SqlDataAdapter adapt = new SqlDataAdapter(command);

                DataSet ds = new DataSet();
                adapt.Fill(ds);
                
                dataGridView1.DataSource = ds.Tables[0];
                dataGridView2.DataSource = ds.Tables[1];
                dataGridView3.DataSource = ds.Tables[2];
                


                connection.Close();
            }
                //using (frmWaitForm frm = new frmWaitForm(SaveData))
                //{
                //    frm.ShowDialog(this);
                    // add row number 
                    //foreach (DataGridViewRow row in dataGridView1.Rows)
                    //    row.HeaderCell.Value = (row.Index + 1).ToString();

                    //foreach (DataGridViewRow row in dataGridView2.Rows)
                    //    row.HeaderCell.Value = (row.Index + 1).ToString();

                    //foreach (DataGridViewRow row in dataGridView3.Rows)
                    //    row.HeaderCell.Value = (row.Index + 1).ToString();

                    //this.dataGridView1.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    //this.dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                    //this.dataGridView1.AutoResizeColumns();

                    //this.dataGridView2.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    //this.dataGridView2.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                    //this.dataGridView2.AutoResizeColumns();

                    //this.dataGridView3.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    //this.dataGridView3.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                    //this.dataGridView3.AutoResizeColumns();

                //}

            //}
           

        }

      
       

        private void btnBackupTable_Click(object sender, EventArgs e)
        {


            // keep two versions of copies for each tables, table_back, table_back_prev

            string queryString = @"if (OBJECT_ID('dbo.task_workgroup_mstr_Back','U') IS NULL and OBJECT_ID('dbo.task_workgroup_mstr_Back_prev','U')IS NULL)
	                                select * into dbo.task_workgroup_mstr_Back from  dbo.task_workgroup_mstr 
                                    else if (OBJECT_ID('task_workgroup_mstr_Back','U') IS NOT NULL and OBJECT_ID('task_workgroup_mstr_Back_prev','U') IS NULL)
	                                begin
	                                    select * into task_workgroup_mstr_Back_prev from task_workgroup_mstr_Back
                                       drop table task_workgroup_mstr_Back
                                       select * into task_workgroup_mstr_Back from task_workgroup_mstr 
                                    end
                                    else IF (OBJECT_ID('dbo.task_workgroup_mstr_Back','U') IS NOT NULL and OBJECT_ID('dbo.task_workgroup_mstr_Back_prev','U') IS NOT NULL)
                                      begin
                                       drop table dbo.task_workgroup_mstr_Back_prev
                                       select * into dbo.task_workgroup_mstr_Back_prev from dbo.task_workgroup_mstr_Back
                                       drop table dbo.task_workgroup_mstr_Back
                                       select * into dbo.task_workgroup_mstr_Back from dbo.task_workgroup_mstr 
                                       END

                                    if (OBJECT_ID('dbo.task_workgroup_assigned_to_Back','U') IS NULL and OBJECT_ID('dbo.task_workgroup_assigned_to_Back_prev','U') IS NULL)
	                                select * into dbo.task_workgroup_assigned_to_Back from dbo.task_workgroup_assigned_to 
                                    else if (OBJECT_ID('dbo.task_workgroup_assigned_to_Back','U') IS NOT NULL and OBJECT_ID('dbo.task_workgroup_assigned_to_Back_prev','U')IS NULL)
	                                begin
	                                    select * into dbo.task_workgroup_assigned_to_Back_prev from dbo.task_workgroup_assigned_to_Back
                                       drop table dbo.task_workgroup_assigned_to_Back
                                       select * into dbo.task_workgroup_assigned_to_Back from dbo.task_workgroup_assigned_to 
                                    end
                                    else IF (OBJECT_ID('dbo.task_workgroup_assigned_to_Back','U') IS NOT NULL and OBJECT_ID('dbo.task_workgroup_assigned_to_Back_prev','U') IS NOT NULL)
                                      begin
                                       drop table dbo.task_workgroup_assigned_to_Back_prev
                                       select * into dbo.task_workgroup_assigned_to_Back_prev from dbo.task_workgroup_assigned_to_Back
                                       drop table dbo.task_workgroup_assigned_to_Back
                                       select * into dbo.task_workgroup_assigned_to_Back from dbo.task_workgroup_assigned_to 
                                       END


                                  if (OBJECT_ID('dbo.task_workgroup_user_xref_Back','U') IS NULL and OBJECT_ID('dbo.task_workgroup_user_xref_Back_prev','U') IS NULL)
	                            select * into dbo.task_workgroup_user_xref_Back from dbo.task_workgroup_user_xref 

                                else if (OBJECT_ID('dbo.task_workgroup_user_xref_Back','U') IS NOT NULL and OBJECT_ID('dbo.task_workgroup_user_xref_Back_prev','U') IS NULL)
	                            begin
	                            select * into dbo.task_workgroup_user_xref_Back_prev from dbo.task_workgroup_user_xref_Back
                                drop table dbo.task_workgroup_user_xref_Back
                                select * into dbo.task_workgroup_user_xref_Back from dbo.task_workgroup_user_xref 
                                end

                                else IF (OBJECT_ID('dbo.task_workgroup_user_xref_Back','U') IS NOT NULL and OBJECT_ID('dbo.task_workgroup_user_xref_Back_prev','U') IS NOT NULL)
                                begin
                               drop table dbo.task_workgroup_user_xref_Back_prev
                               select * into dbo.task_workgroup_user_xref_Back_prev from dbo.task_workgroup_user_xref_Back
                               drop table dbo.task_workgroup_user_xref_Back
                               select * into dbo.task_workgroup_user_xref_Back from dbo.task_workgroup_user_xref 
                               END";


            String system = DataSystem.Substring(0, 6);

         // using (frmWaitForm frm = new frmWaitForm(SaveData))
           // {
               // frm.ShowDialog(this);

                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[DataSystem].ConnectionString))
                {
                    //backgroundWorker1.WorkerReportsProgress = true;
                    // backgroundWorker1.RunWorkerAsync();

                    connection.Open();
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.ExecuteNonQuery();

                    MessageBox.Show("Three tables were backup in " + system + ".");
                }

               RefreshDataGrid();
               

          //  }

            btnBackupTable.Enabled = false;
            btnTruncate.Enabled = true;
            btnCopy.Enabled = false;

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnTruncate_Click(object sender, EventArgs e)
        {
            string queryString = @" Truncate table dbo.task_workgroup_assigned_to
                                    Truncate table dbo.task_workgroup_mstr
                                    Truncate table dbo.task_workgroup_user_xref";

           
            String system = DataSystem.Substring(0, 6);

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[DataSystem].ConnectionString))
            {

                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                command.ExecuteNonQuery();

                MessageBox.Show("Three tables were truncated in " + system + ".");
            }

            RefreshDataGrid();


            btnBackupTable.Enabled = false;
            btnTruncate.Enabled = false;
            btnCopy.Enabled = true;

           

        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            string queryString = @" INSERT INTO [dbo].[task_workgroup_mstr]
                                   ([enterprise_id]
                                   ,[practice_id]
                                   ,[workgroup_id]
                                   ,[workgroup_name]
                                   ,[workgroup_desc]
                                   ,[note]
                                   ,[delete_ind]
                                   ,[create_timestamp]
                                   ,[created_by]
                                   ,[modify_timestamp]
                                   ,[modified_by]
                                   ,[former_group_id]
                                   ,[workgroup_type])
                            SELECT [enterprise_id]
                                   ,[practice_id]
                                   ,[workgroup_id]
                                   ,[workgroup_name]
                                   ,[workgroup_desc]
                                   ,[note]
                                   ,[delete_ind]
                                   ,[create_timestamp]
                                   ,[created_by]
                                   ,[modify_timestamp]
                                   ,[modified_by]
                                   ,[former_group_id]
                                   ,[workgroup_type]
	                              FROM  [dc-nexgn-db-01].[NGprod].[dbo].[task_workgroup_mstr]

                                 
                                  INSERT INTO [dbo].[task_workgroup_user_xref]
                                   ([workgroup_id]
                                   ,[user_id]
                                   ,[created_by]
                                   ,[create_timestamp])
                                    SELECT [workgroup_id]
                                   ,[user_id]
                                   ,[created_by]
                                   ,[create_timestamp]
		                          FROM  [dc-nexgn-db-01].[NGprod].[dbo].[task_workgroup_user_xref]

                                  INSERT INTO [dbo].[task_workgroup_assigned_to]
                                    ([practice_id]
                                   ,[task_id]
                                   ,[workgroup_id]
                                   ,[create_timestamp]
                                   ,[created_by]
                                   ,[modify_timestamp]
                                   ,[modified_by]
                                   ,[response_type]
                                   ,[enterprise_id])
                             SELECT [practice_id]
                                   ,[task_id]
                                   ,[workgroup_id]
                                   ,[create_timestamp]
                                   ,[created_by]
                                   ,[modify_timestamp]
                                   ,[modified_by]
                                   ,[response_type]
                                   ,[enterprise_id]
		                          FROM  [dc-nexgn-db-01].[NGprod].[dbo].[task_workgroup_assigned_to]";


            String system = DataSystem.Substring(0, 6);

            //using (frmWaitForm frm = new frmWaitForm(SaveData))
            //{
            //    frm.ShowDialog(this);

                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[DataSystem].ConnectionString))
                {

                    connection.Open();
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.ExecuteNonQuery();

                    MessageBox.Show("Three tables were copied from prod in " + system + ".");
                }





            RefreshDataGrid();
            //}
            if (DataSystem == "NGDEVLConnString")
            {
                devl_copy = 1;
                radioButton2.Checked = false;
                radioButton1.Checked = true;
            }
            else
            {
                test_copy = 1;
                radioButton1.Checked = false;
                radioButton2.Checked = true;

            }

            btnBackupTable.Enabled = false;
            btnTruncate.Enabled = false;
            btnCopy.Enabled = false;

           


            // log user info
            //(string formName, string action, string file_name, string provider_name, string provider_id, string note1, string note2, string note3)
            Commons.LogUser2(this.Name, "Copy Taskworkgroup tables to " + system, "","","","","","");

        }

       

        public void frmTaskWorkgroup_FormClosing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {

            if ((test_copy == 1 && devl_copy == 0) || (test_copy == 0 && devl_copy == 1))
            {
                MessageBox.Show("You need to copy both systems.");
                e.Cancel = true;
            }
            else if (test_copy == 0 && devl_copy == 0 && btnCopy.Enabled)
            {
                MessageBox.Show("After you truncate tables, you need to click step 3 to copy tables.");
                e.Cancel = true;
            }
        }

        //private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        //{
        //    for (int i = 1; i <= 100; i++)
        //    {
        //        // Wait 50 milliseconds.  
        //        Thread.Sleep(50);
        //        // Report progress.  
        //        backgroundWorker1.ReportProgress(i);
        //    }
        //}

        //private void backgroundWorker1_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        //{
        //    // Change the value of the ProgressBar   
        //    progressBar2.Value = e.ProgressPercentage;
        //    // Set the text.  
        //    this.Text = e.ProgressPercentage.ToString();
        //}

        //private void frmTaskWorkgroup_Load(object sender, System.EventArgs e)
        //{
        //    backgroundWorker1.WorkerReportsProgress = true;
        //    backgroundWorker1.RunWorkerAsync();
        //}
        // 
        //void SaveData()
        //{
        //    //Add code to process your data
        //    for (int i = 0; i <= 500; i++)
        //        Thread.Sleep(10); //Simulator
        //}

        //devl check
        private void radioButton1_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                if (devl_copy == 0 && test_copy == 0)
                {
                    MessageBox.Show("You need to complete DEVL before change to TEST system.");
                    radioButton1.Checked = true;
                    radioButton2.Checked = false;
                    DataSystem = "NGDEVLConnString";
                }

                else if (devl_copy == 1 && test_copy == 0)
                {
                    if (btnBackupTable.Enabled == true)
                    {
                        MessageBox.Show("You need to back up test system.");
                        radioButton1.Checked = false;
                        radioButton2.Checked = true;
                    }
                    else if (btnTruncate.Enabled == true)
                    {
                        MessageBox.Show("You need to truncate test system.");
                        radioButton1.Checked = false;
                        radioButton2.Checked = true;
                    }
                    else if (btnCopy.Enabled == true)
                    {
                        MessageBox.Show("You need to copy test system.");
                        radioButton1.Checked = false;
                        radioButton2.Checked = true;
                    }

                    DataSystem = "NGTESTConnString";


                }

                else if (devl_copy == 1 && test_copy == 1)
                {
                    dataGridView1.DataSource = null;
                    dataGridView2.DataSource = null;
                    dataGridView3.DataSource = null;
                    MessageBox.Show("You have completed both systems and you can close this application.");
                }
            } 
        }

        //DEVL
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
           
        }
        // TEST check
        private void radioButton2_Click(object sender, EventArgs e)
        {

            if (radioButton2.Checked == true)
            {
                if (devl_copy == 0 && test_copy == 0)
                {
                    MessageBox.Show("You need to complete DEVL before change TEST system.");
                    radioButton2.Checked = false;
                    radioButton1.Checked = true;
                    DataSystem = "NGDEVLConnString";

                }
                else if (devl_copy == 1 && test_copy == 0)
                {

                    radioButton1.Checked = false;
                    radioButton2.Checked = true;
                    DataSystem = "NGTESTConnString";

                    dataGridView1.DataSource = null;
                    dataGridView2.DataSource = null;
                    dataGridView3.DataSource = null;

                    btnBackupTable.Enabled = true;
                    btnTruncate.Enabled = false;
                    btnCopy.Enabled = false;
                    //MessageBox.Show("test.");
                }

                //    //if (DataSystem == "NGDEVLConnString")
                //    //{
                //    //    if (btnBackupTable.Enabled == false)
                //    //    {
                //    //        MessageBox.Show("Please complete all steps before move to TEST database.");
                //    //        radioButton2.Checked = false;
                //    //        radioButton1.Checked = true;
                //    //    }
                //    //    else
                //    //    {
                //    //        btnBackupTable.Enabled = true;
                //    //        btnTruncate.Enabled = false;
                //    //        btnCopy.Enabled = false;
                //    //        DataSystem = "NGTestConnString";
                //    //    }
                //    //}
                //}
                else if (devl_copy == 1 && test_copy == 1)
                {
                    if (btnCopy.Enabled == false)
                    {
                        MessageBox.Show("You have completed both systems and you can close this app.");
                        radioButton2.Checked = false;
                        radioButton1.Checked = false;
                    }

                }
            }

        }

       

        //// button2 TEST
        private void radioButton2_CheckedChanged(object sender, EventArgs e)//TEST
        {
        }

        private void button5_Click(object sender, EventArgs e)
        {
        }
    }
}
