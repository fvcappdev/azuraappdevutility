USE [NGTest]
GO

/****** Object:  Table [dbo].[AzuraUtilyUserAudit]    Script Date: 4/14/2021 10:48:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AzuraUtilyUserAudit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NULL,
	[Form] [varchar](500) NULL,
	[Action] [varchar](100) NULL,
	[provider_name] [varchar](100) NULL,
	[provider_id] [varchar](100) NULL,
	[file_name] [varchar](100) NULL,
	[DateStamp] [datetime] NOT NULL,
	[note1] [varchar](250) NULL,
	[note2] [varchar](500) NULL,
	[note3] [varchar](1000) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[AzuraUtilyUserAudit] ADD  DEFAULT (getdate()) FOR [DateStamp]
GO


