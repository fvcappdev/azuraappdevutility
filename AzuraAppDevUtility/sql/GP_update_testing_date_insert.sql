-- GP update query to get testing data
-- first query, 
--anything not in B3970300 B, first comment isnull(B.POPRCTNM, '') = '' get list of 
-- then delte them from B3970300
select RH.POPRCTNM, 
(select top 1 LOCNCODE from POP30310 where POPRCTNM = RH.POPRCTNM) as FACILITY,
	(select top 1 PONUMBER from POP30310 where POPRCTNM = RH.POPRCTNM) as PONUMBER, 
	RH.receiptdate, RH.POSTEDDT, 
	RH.VENDORID, 
	RH.VENDNAME
	,B.POPRCTNM
from POP30300 RH
left join B3970300 B on
	RH.POPRCTNM = B.POPRCTNM
where (RH.receiptdate between '3/1/2021' and '3/31/2021') and 
	RH.POPTYPE = 1 
	and isnull(B.POPRCTNM, '') = ''
order by RH.receiptdate

-- delte these 
-- select *
delete from  B3970300 
where POPRCTNM in (
'RCT1621044',      
'RCT1621045',       
'RCT1621143',       
'RCT1621151',       
'RCT1621183'       )     



-- second query update
select RH.POPRCTNM, 
(select top 1 LOCNCODE from POP30310 where POPRCTNM = RH.POPRCTNM) as FACILITY,
	(select top 1 PONUMBER from POP30310 where POPRCTNM = RH.POPRCTNM) as PONUMBER, RH.receiptdate, RH.POSTEDDT, 
	RH.VENDORID, RH.VENDNAME
from POP30300 RH
inner join B3970300 B on
	RH.POPRCTNM = B.POPRCTNM and
	RH.POPTYPE = B.POTYPE
where (RH.receiptdate between '3/1/2021' and '3/31/2021') and
	RH.POPTYPE = 1 and 
	((isnull(B.BSSI_Facility_ID, '') = '') or (rtrim(ltrim(B.BSSI_Facility_ID)) = ''))
order by RH.receiptdate


update B3970300
set BSSI_Facility_ID =''
where POPRCTNM in ('RCT1621312       ','RCT1621312       ','RCT1620966       ','RCT1620991       ')

select  LOCNCODE,* from POP30310 where POPRCTNM='RCT162273�����mF����_