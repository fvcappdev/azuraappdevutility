use [test]

 --insert [dbo].[B3970300_BAK1]
 --select * from [PowerBI_Testing].[dbo].[B3970300]

 delete from [dbo].[B3970300]

insert into [dbo].[B3970300]
([POPRCTNM] ,
	[POTYPE] ,
	[BSSI_Rcpt_TypeID] ,
	[BSSI_Facility_ID] ,
	[BSSI_Department_ID] ,
	[BSSI_Allocation_ID] ,
	[BSSI_Custom_Dist] ,
	[BSSI_Voucher_Address] )
	select
	[POPRCTNM] ,
	[POTYPE] ,
	[BSSI_Rcpt_TypeID] ,
	[BSSI_Facility_ID] ,
	[BSSI_Department_ID] ,
	[BSSI_Allocation_ID] ,
	[BSSI_Custom_Dist] ,
	[BSSI_Voucher_Address] 
	from --[dc-dwh-db-01].[PowerBI_Testing].[dbo].[B3970300_BAK1]
	[AAC].[dbo].[B3970300_BAK1]


 select count(*) from [dbo].[B3970300]
    select count(*) from [dbo].[B3970300_BAK1]

select count(*) from [dbo].[POP30300]
select count(*) from [dbo].[POP30310]

--insert [dbo].[POP30300]
/*
select * into [dbo].[POP30300_bak] 
from [aacgrgp].[aac].[dbo].[POP30300]
where [receiptdate] between '4/1/2021' and '4/30/2021'
*/
insert [POP30310]
select * into [POP30310_bak]
 from [aacgrgp].[aac].[dbo].[POP30310]