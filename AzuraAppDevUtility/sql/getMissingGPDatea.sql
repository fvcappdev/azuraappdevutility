select RH.POPRCTNM, (select top 1 LOCNCODE from POP30310 where POPRCTNM = RH.POPRCTNM) as FACILITY,
	(select top 1 PONUMBER from POP30310 where POPRCTNM = RH.POPRCTNM) as PONUMBER, RH.receiptdate, RH.POSTEDDT, 
	RH.VENDORID, RH.VENDNAME
	,B.POPRCTNM
from POP30300 RH
left join B3970300 B on
	RH.POPRCTNM = B.POPRCTNM
where (RH.receiptdate between '1/1/2016' and '1/31/2016') and 
	RH.POPTYPE = 1  
	and isnull(B.POPRCTNM, '') = ''
order by RH.receiptdate

 update B3970300 make POPRCTNM, '') = ''
update B3970300
set POPRCTNM=''
where POPRCTNM='RCT1109224       '

/* Get all Receipts that have a corresponding record in B3970300 where
	receiptdate > @sinceRhRctDate and POPTYPE = 1 and
	BSSI_Facility_ID is empty or null
*/	
select RH.POPRCTNM, (select top 1 LOCNCODE from POP30310 where POPRCTNM = RH.POPRCTNM) as FACILITY,
	(select top 1 PONUMBER from POP30310 where POPRCTNM = RH.POPRCTNM) as PONUMBER, RH.receiptdate, RH.POSTEDDT, 
	RH.VENDORID, RH.VENDNAME
	,B.BSSI_Facility_ID
	,B.POTYPE
from POP30300 RH
inner join B3970300 B on
	RH.POPRCTNM = B.POPRCTNM and
	RH.POPTYPE = B.POTYPE
where (RH.receiptdate between '1/1/2016' and '1/31/2016') and
	RH.POPTYPE = 1 
	and ((isnull(B.BSSI_Facility_ID, '') = '') or (rtrim(ltrim(B.BSSI_Facility_ID)) = ''))
order by RH.receiptdate

update B3970300
set BSSI_Facility_ID=''
where BSSI_Facility_ID='8670'                                                                                                                                                                                                                                              
and POPRCTNM='RCT622337        '    
and POTYPE  ='1'

select * from  B3970300
where BSSI_Facility_ID='8670'                                                                                                                                                                                                                                              
and POPRCTNM='RCT622337        '    
and POTYPE  ='1'                                                                                       