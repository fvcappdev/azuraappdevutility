--use TEST_DB
--go

SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[UtilUsers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[user_name] [varchar](50) NULL,
	[active] [int],
	[DateCreated] [datetime] NOT NULL DEFAULT getdate()
) ON [PRIMARY]


create TABLE [dbo].[AzuraUtilyUserAudit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NULL,
	[Form] [varchar](500) NULL,
	[Action] [varchar](100) NULL,
	[provider_name] [varchar](100) NULL,
	[provider_id] [varchar](100) NULL,
	[file_name] [varchar](100) NULL,
	[DateStamp] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[AzuraUtilyUserAudit] ADD  DEFAULT (getdate()) FOR [DateStamp]
GO
